const $closed = document.getElementById("namekit-closed")
const $namekit = document.getElementById("namekit")

$closed.addEventListener("click", () => {
  Setup(ROMS, SAVE_STATES);
  Frames.show("namekit");
  $closed.classList.add("hidden");
})

$namekit.addEventListener("hide-frame", () => {
  $closed.classList.remove("hidden");
})

import './lib/jsnes.js'
import downloadObjectAsJson from './lib/downloadObjectAsJson.js'

import NESsampler from './NESsampler/NESsampler.js'
import NEStrack from './NESsampler/NEStrack.js'
import DevTools from './NESsampler/DevTools.js'

import KEYS from './NESsampler/inputMaps/onePlayerDefault.js'

const ROMS = {
  dice: 'dice2.nes',
  blob: 'blob3.nes', // TODO: run gimmick
  rock: 'rock2.nes',
  ball: 'ball1.nes', // could be golf
  letter: 'letters-elmo.nes',
  // wheel: 'wheel.nes', // wheel of fortune
  wheel: 'blocksfall.nes',
  suits: 'link.nes',
  stamp: 'gungame1.nes'

  // pinball acho que é uma boa
  // blob: 'blob2.nes', // TODO: run gimmick
}

const SAVE_STATES = {
  dice: 'dice2-gameplay.json',
  letter: 'letter-die-spinning-wheel.json',
  suits: 'link-gameplay.json',
  blob: 'blob3-namekit.json',
  rock: 'rock2-namekit.json',
  ball: 'ball1-gameplay.json',
}

function bgr2rgb(color) {
  return ((0xff0000 & color) >> 0x10) | (0xff00 & color) | ((0xff & color) << 0x10)
}

window.currentMemTest = 'eq'
window.checkMem = false
window.filterMem = null
// window.filterMem = window.localStorage.getItem('filtermem') || null
// if(window.filterMem != null) {
  // window.filterMem = JSON.parse(window.filterMem)
// }

let prevMem = 0;
function Compare(a, b, op) {
  switch(op) {
    case 'leq':
      return a <= b
      break;
    case 'geq':
      return a >= b
      break;
    case 'gt':
      return a > b
      break;
    case 'lt':
      return a < b
      break;
    case 'neq':
      return a != b
      break;
    case 'eq':
      return a == b
      break;
  }

  console.error(`${op} does not exist`);
  return true
}

function DoMemoryCheck(track) {
  const mem = track.nes.cpu.mem
  if(window.filterMem == null) {
    window.filterMem = mem.map((m, i) => {
      return [m, null]
    })
  } else {
    window.filterMem = window.filterMem.map((m, i) => {
      if(m == null) return null;
      if(Compare(m[0], mem[i], window.currentMemTest)) {
        return [mem[i], mem[i] - m[0]]
      } else {
        return null
      }
    })

    const mappedMem = window.filterMem.reduce((p, c, i) => {
      if(c != null) {
        p[i.toString(16)] = c
      }
      return p
    }, {})

    console.log(mappedMem)
  }
}

let focusedTrack
const main = (roms, savestates) => {
  const SCREEN_WIDTH = 800
  const SCREEN_HEIGHT = 800


  // const nesSamplerLink = new NESampler()

  function KitSampler(dumplingId, sample) {
    const $dumpling = document.getElementById(dumplingId)
    const $container = $dumpling.querySelector(".game-screen")
    const containerId = $container.id
    const sampler = new NESsampler(SCREEN_WIDTH, SCREEN_HEIGHT, containerId)

    const track = new NEStrack(
    { // parameters
      name: dumplingId,
      x: 0,
      y: 0,
      width: SCREEN_WIDTH,
      height: SCREEN_HEIGHT,
      hasControl: true,
      controller: KEYS,
    },
    function update() {
    },
    function shader(buffer, x, y) { // shader
      let ri = (y) * 256 + (x)
      return 0xff000000 | buffer[ri]
    },
    sample
    )

    sampler.tracks.push(track)
    track.pause()

    $dumpling.addEventListener("focus-frame", () => {
      focusedTrack = track
      track.unpause()
    })

    $dumpling.addEventListener("show-frame", () => {
      track.unpause()
    })

    $dumpling.addEventListener("unfocus-frame", () => {
      track.pause()
    })
    $dumpling.addEventListener("hide-frame", () => {
      track.pause()
    })

    return sampler
  }

  const diceSampler = KitSampler("die",  {
      // sample
      rom: roms.dice,
      initialState: savestates.dice,
      // restartTrigger: function() {
        // return !gameData.eater.alive
        // return this.nes.cpu.load(gameData.eater.ramLives) < 2
      // },
      // onRestart: function() {
      // }
    })
  const wheelSampler = KitSampler("wheel",  {
      // sample
      rom: roms.wheel,
      // initialState: savestates.wheel,
      // restartTrigger: function() {
        // return !gameData.eater.alive
        // return this.nes.cpu.load(gameData.eater.ramLives) < 2
      // },
      // onRestart: function() {
      // }
    })
  const stoneSampler = KitSampler("stone",  {
      // sample
      rom: roms.rock,
      initialState: savestates.rock,
      // restartTrigger: function() {
        // return !gameData.eater.alive
        // return this.nes.cpu.load(gameData.eater.ramLives) < 2
      // },
      // onRestart: function() {
      // }
    })
    const ballSampler = KitSampler("ball",  {
      // sample
      rom: roms.ball,
      initialState: savestates.ball,
      // restartTrigger: function() {
        // return !gameData.eater.alive
        // return this.nes.cpu.load(gameData.eater.ramLives) < 2
      // },
      // onRestart: function() {
      // }
    })
  const letterSampler = KitSampler("letter-die",  {
      // sample
      rom: roms.letter,
      initialState: savestates.letter,
      // restartTrigger: function() {
        // return !gameData.eater.alive
        // return this.nes.cpu.load(gameData.eater.ramLives) < 2
      // },
      // onRestart: function() {
      // }
    })
  const heartsSampler = KitSampler("suited-die",  {
      // sample
      rom: roms.suits,
      initialState: savestates.suits,
      // restartTrigger: function() {
        // return !gameData.eater.alive
        // return this.nes.cpu.load(gameData.eater.ramLives) < 2
      // },
      // onRestart: function() {
      // }
    })
  const rubberObjectSampler = KitSampler("rubber-object",  {
      // sample
      rom: roms.blob,
      initialState: savestates.blob,
      // restartTrigger: function() {
        // return !gameData.eater.alive
        // return this.nes.cpu.load(gameData.eater.ramLives) < 2
      // },
      // onRestart: function() {
      // }
    })
  const rubberStampSampler = KitSampler("rubber-stamp",  {
      // sample
      rom: roms.stamp,
      // initialState: savestates.stamp,
      // restartTrigger: function() {
        // return !gameData.eater.alive
        // return this.nes.cpu.load(gameData.eater.ramLives) < 2
      // },
      // onRestart: function() {
      // }
    })

  rubberStampSampler.loop()
  diceSampler.loop()
  rubberObjectSampler.loop()
  stoneSampler.loop()
  wheelSampler.loop()
  ballSampler.loop()
  letterSampler.loop()
  heartsSampler.loop()


  diceSampler.canvasMod.addEventListener('pointerdown', (e) => {
  })

  DevTools(() => focusedTrack)

  }

function Setup(roms, savestates, callback) {
  const downloadedRoms = {}
  const downloadedSaves = {}
  for(const romKey of Object.keys(roms)) {
    const romName = roms[romKey]
    let xhr = new XMLHttpRequest();
    xhr.open('GET', `./raw_games/${romName}`);
    xhr.overrideMimeType("text/plain; charset=x-user-defined");
    xhr.send();

    xhr.onload = function() {
      if (xhr.status != 200) { // analyze HTTP status of the response
        console.error(`Error ${xhr.status}: ${xhr.statusText}`); // e.g. 404: Not Found
      } else { // show the result
        // alert(`Done, got ${xhr.response.length} bytes`); // response is the server response
        downloadedRoms[romKey] = xhr.responseText
        onLoad()
      }
    }
  }

  for(const saveKey of Object.keys(savestates)) {
    const saveStateName = savestates[saveKey]
    let xhr2 = new XMLHttpRequest();
    xhr2.open('GET', `./save_states/${saveStateName}`);
    // xhr2.overrideMimeType("text/plain; charset=x-user-defined");
    xhr2.send();
    xhr2.onload = function() {
      if (xhr2.status != 200) { // analyze HTTP status of the response
        console.error(`Error ${xhr2.status}: ${xhr2.statusText}`); // e.g. 404: Not Found
      } else { // show the result
        const saveState = JSON.stringify(JSON.parse(xhr2.responseText))
        downloadedSaves[saveKey] = saveState
        onLoad()
      }
    }
  }

  function onLoad() {
    if(Object.keys(downloadedSaves).length < Object.keys(savestates).length) return;
    if(Object.keys(downloadedRoms).length < Object.keys(roms).length) return;
    main(downloadedRoms, downloadedSaves)
  }
}