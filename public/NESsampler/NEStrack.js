const NES_WIDTH = 256
const NES_HEIGHT = 240

const trackRestartEvent = new Event('track.restarted');

export default class NEStrack {
  static WIDTH = NES_WIDTH
  static HEIGHT = NES_HEIGHT

  prevMem = []
  drawSettings = null
  frame = 0
  loaded = false
  savedStates = {}

  constructor(settings, update, shader, sample) {
    // #region Canvas bullshit
    this.canvas = document.createElement('canvas')
    this.canvas.width = NES_WIDTH
    this.canvas.height = NES_HEIGHT
    this.context = this.canvas.getContext('2d')
    this.imageDataTrack = this.context.getImageData(0, 0, NES_WIDTH, NES_HEIGHT)
    this.context.fillStyle = 'black'
    // set alpha to opaque
    this.context.fillRect(0, 0, 256, 240)

    // buffer to write on next animation frame
    this.bufTrack = new ArrayBuffer(this.imageDataTrack.data.length)
    this.buf8Track = new Uint8ClampedArray(this.bufTrack)
    this.buf32Track = new Uint32Array(this.bufTrack)

    // Set alpha
    for (let i = 0; i < this.buf32Track.length; ++i) {
      this.buf32Track[i] = 0xffff00ff
    }
    // #endregion

    this.sample = sample
    this._update = update && update.bind(this) || false
    this._shader = shader && shader.bind(this) || false

    this.drawSettings = settings

    this.nes = new jsnes.NES({
      onFrame: (buffer) => {
        for (let i = 0; i < this.buf32Track.length; ++i) {
          this.buf32Track[i] = buffer[i]
        }
        this.frame++
      },
      onAudioSample: function (left, right) {
      }
    })

    this.canControl = settings.hasControl
    if (settings.hasControl) {
      // TODO: send all keyups when losing control
      document.addEventListener('keyup', (e) => {
        if(!this.canControl) return;
        const key = settings.controller[e.keyCode]
        if (key) {
          this.nes.buttonUp(key[0], key[1])
          // e.preventDefault()
        }
      })
      document.addEventListener('keydown', (e) => {
        if(!this.canControl) return;
        const key = settings.controller[e.keyCode]
        if (key) {
          this.nes.buttonDown(key[0], key[1])
          // e.preventDefault()
        }
      })
    }

    // console.log('loading rom...')
    this.nes.loadROM(sample.rom)
    this.nes.frame()
    // console.log('loaded rom!')
    if (sample) {
      // console.log("loading sample...") //, sample)

      if(sample.initialState) {
        this.nes.fromJSON(JSON.parse(sample.initialState))
      }

      sample.initialState = JSON.stringify(this.nes.toJSON())

      if(sample.restartTrigger) {
        this.restartTrigger = sample.restartTrigger.bind(this)
      }

      if(sample.onRestart) {
        this.onRestart = sample.onRestart.bind(this)
      }

      if (sample.loopTime) {
        setInterval(() => {
          this.nes.fromJSON(JSON.parse(sample.initialState))
        }, sample.loopTime)
      }

      // console.log('loaded sample!')
    }

    this.isPlaying = true
    this.loaded = true
  }

  draw(context) {
    const scaleX = this.drawSettings.width / NES_WIDTH
    const scaleY = this.drawSettings.height / NES_HEIGHT

    if (this.isPlaying) {
      if(this._shader) {
        for (let y = 0; y < NES_HEIGHT; ++y) {
          for (let x = 0; x < NES_WIDTH; ++x) {
            let i = (y * 256 + x)
            this.buf32Track[i] = this._shader(this.buf32Track, x, y)
          }
        }
      }

      this.imageDataTrack.data.set(this.buf8Track)
      this.context.putImageData(this.imageDataTrack, 0, 0)
    };

    context.save()
    context.scale(scaleX, scaleY)
    context.drawImage(this.canvas, this.drawSettings.x/scaleX, this.drawSettings.y/scaleY)
    context.restore()
  }

  update(force) {
    if(force || this.isPlaying) {
      // console.log("[track] updating", this.drawSettings.name)
      this.nes.frame()
    }
    if(this._update) this._update()
  }

  executeFrame(context, force, draw=true) {
    this.update(force)
    if (draw) {
      this.draw(context)
    }

    if(this.restartTrigger && this.restartTrigger()) {
        this.canvas.dispatchEvent(trackRestartEvent)
        this.restart()
    }

    this.prevMem = this.nes.cpu.mem.map(a => a)
  }

  restart() {
    if(this.onRestart) {
      this.onRestart()
    }
    if(this.sample) {
      this.nes.fromJSON(JSON.parse(this.sample.initialState))
    } else {
      this.nes.reset()
    }
  }

  unpause() {
    console.log("[track] unpausing", this.drawSettings.name)
    this.isPlaying = true
  }

  pause() {
    console.log("[track] pausing", this.drawSettings.name)
    this.isPlaying = false
  }

  togglePause() {
    console.log("[track] togglingPause", this.drawSettings.name)
    this.isPlaying = !this.isPlaying
  }

  saveState(index = 0) {
    this.savedStates[index] = JSON.stringify(this.nes.toJSON())
  }

  loadState(index = 0) {
    const state = this.savedStates[index]
    if(state) {
      this.nes.fromJSON(JSON.parse(state))
    }
  }

  giveControl() {
    this.canControl = true;
  }

  removeControl() {
    this.canControl = false;
    [
      jsnes.Controller.BUTTON_UP,
      jsnes.Controller.BUTTON_DOWN,
      jsnes.Controller.BUTTON_LEFT,
      jsnes.Controller.BUTTON_RIGHT,
      jsnes.Controller.BUTTON_A,
      jsnes.Controller.BUTTON_B,
      jsnes.Controller.BUTTON_SELECT,
      jsnes.Controller.BUTTON_START,
    ].forEach(k => {
      this.nes.buttonUp(1, k);this.nes.buttonUp(2, k)
    })
  }
}