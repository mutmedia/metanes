import downloadObjectAsJson from '../lib/downloadObjectAsJson.js'

let prevMem = 0;
function Compare(a, b, op) {
  switch(op) {
    case 'leq':
      return a <= b
      break;
    case 'geq':
      return a >= b
      break;
    case 'gt':
      return a > b
      break;
    case 'lt':
      return a < b
      break;
    case 'neq':
      return a != b
      break;
    case 'eq':
      return a == b
      break;
  }

  console.error(`${op} does not exist`);
  return true
}

function DoMemoryCheck(track) {
  const mem = track.nes.cpu.mem
  if(window.filterMem == null) {
    window.filterMem = mem.map((m, i) => {
      return [m, null]
    })
  } else {
    window.filterMem = window.filterMem.map((m, i) => {
      if(m == null) return null;
      if(Compare(m[0], mem[i], window.currentMemTest)) {
        return [mem[i], mem[i] - m[0]]
      } else {
        return null
      }
    })

    const mappedMem = window.filterMem.reduce((p, c, i) => {
      if(c != null) {
        p[i.toString(16)] = c
      }
      return p
    }, {})

    console.log(mappedMem)
  }
}



export default function DevTools (trackGetter) {
  let saveState
  document.addEventListener('keyup', (e) => {
    const track = trackGetter()
    switch (e.keyCode) {
      case 49: // 1 Check memory
        DoMemoryCheck(track)
        break
      case 50: // 2 Save memory
        window.localStorage.setItem(
          'filtermem',
          JSON.stringify(window.filterMem))
        break
      case 51: // 3 Clear memory
        window.localStorage.setItem(
          'filtermem',
          null)
        window.filterMem = null
        break
      case 52: // 4 load state
        if(saveState !== null) {
          track.nes.fromJSON(JSON.parse(saveState))
        }
        break
      case 53: // 5 save state
        saveState = JSON.stringify(track.nes.toJSON());
        break
      case 54: // 6 download state
        downloadObjectAsJson(track.nes.toJSON())
        break
      case 55: // 7
        track.reset()
        break
      case 32: // space
        // window.checkMem = false;
      case 67: // C
        break
      case 48: // 0
        // window.Compare
        if(window.currentMemTest === 'eq') {
          window.currentMemTest = 'neq'
        } else {
          window.currentMemTest = 'eq'
        }
        console.log("CURRENT OP", window.currentMemTest)

        break;
      case 79: // O
        track.executeFrame(track.context, true)
        break
      case 80: // P
        track.togglePause()
        break
      case 82: // R
        // track.nes.fromJSON(JSON.parse(GetFrame(track.frame, 30)))
        track.restart();
        // CreateKillTrack()
        break
      }
    })
}