import '../../lib/jsnes.js'
export default {
  38: [1, jsnes.Controller.BUTTON_UP], // Up
  40: [1, jsnes.Controller.BUTTON_DOWN], // Down
  37: [1, jsnes.Controller.BUTTON_LEFT], // Left
  39: [1, jsnes.Controller.BUTTON_RIGHT], // Right

  87: [1, jsnes.Controller.BUTTON_UP], // W
  83: [1, jsnes.Controller.BUTTON_DOWN], // S
  65: [1, jsnes.Controller.BUTTON_LEFT], // A
  68: [1, jsnes.Controller.BUTTON_RIGHT], // D

  88: [1, jsnes.Controller.BUTTON_A], // X
  89: [1, jsnes.Controller.BUTTON_B], // Y (Central European keyboard)
  90: [1, jsnes.Controller.BUTTON_B], // Z
  17: [1, jsnes.Controller.BUTTON_SELECT], // Right Ctrl
  8: [1, jsnes.Controller.BUTTON_SELECT], // Backspace
  13: [1, jsnes.Controller.BUTTON_START], // Enter

  190: [1, jsnes.Controller.BUTTON_A], // X
  191: [1, jsnes.Controller.BUTTON_B], // Z
  32: [1, jsnes.Controller.BUTTON_B], // Z

  // 79: [1, jsnes.Controller.BUTTON_A], // O
  // 80: [1, jsnes.Controller.BUTTON_B], // P

  // 87: [2, jsnes.Controller.BUTTON_UP], // W
  // 83: [2, jsnes.Controller.BUTTON_DOWN], // S
  // 65: [2, jsnes.Controller.BUTTON_LEFT], // A
  // 68: [2, jsnes.Controller.BUTTON_RIGHT], // D
  // 13: [2, jsnes.Controller.BUTTON_START], // Enter
  67: [2, jsnes.Controller.BUTTON_A], // C
  // 86: [2, jsnes.Controller.BUTTON_B], // V
  // 88: [2, jsnes.Controller.BUTTON_A], // X
  // 89: [2, jsnes.Controller.BUTTON_B], // Z
}