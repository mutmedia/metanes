function Compare(a, b, op) {
  switch(op) {
    case 'leq':
      return a <= b
      break;
    case 'geq':
      return a >= b
      break;
    case 'gt':
      return a > b
      break;
    case 'lt':
      return a < b
      break;
    case 'neq':
      return a != b
      break;
    case 'eq':
      return a == b
      break;
  }

  console.error(`${op} does not exist`);
  return true
}


function DoMemoryCheck(track) {
  const mem = track.nes.cpu.mem
  if(window.filterMem == null) {
    window.filterMem = mem.map((m, i) => {
      return [m, null]
    })
  } else {
    window.filterMem = window.filterMem.map((m, i) => {
      if(m == null) return null;
      if(Compare(m[0], mem[i], window.currentMemTest)) {
        return [mem[i], mem[i] - m[0]]
      } else {
        return null
      }
    })

    const mappedMem = window.filterMem.reduce((p, c, i) => {
      if(c != null) {
        p[i.toString(16)] = c
      }
      return p
    }, {})

    console.log(mappedMem)
  }
}