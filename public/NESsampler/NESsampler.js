const GAME_FPS = 60
const INTERVAL = 1000 / GAME_FPS

export default class NESsampler {
  // public fields
  contextMod = null

  canvasMod = null

  constructor(width, height, canvasElementId = 'screen_mod') {
    this.canvasMod = document.getElementById(canvasElementId)
    this.contextMod = this.canvasMod.getContext('2d')
    this.canvasMod.style.display = 'block'

    //vish
    const $loading = document.getElementById('loading');
    if($loading) $loading.style.display = 'none'

    const $container = document.getElementById('videoGameContainer')
    if($container) $container.style.display = 'block'

    this.contextMod.fillStyle = 'blue'
    // set alpha to opaque
    this.contextMod.fillRect(0, 0, width, height)
    this.tracks = []
    this.lastFrameTime = false

    this.frame = this.frame.bind(this)
  }

  createTrack() {

  }

  frame(time) {
    // https://github.com/bfirsh/jsnes-web/blob/master/src/FrameTimer.js
    window.requestAnimationFrame(this.frame);
    // how many ms after 60fps frame time
    const excess = time % INTERVAL

    // align to frame interval
    const currentTime = time - excess

    // first frame, ignore
    if (!this.lastFrameTime) {
      this.lastFrameTime = currentTime
      return
    }

    const numFrames = Math.round((currentTime - this.lastFrameTime) / INTERVAL)
    // This can happen a lot on a 144Hz display
    if (numFrames === 0) {
      //console.log("WOAH, no frames");
      return;
    }

    // draw first frame
    this.executeFrame(true)
    this.lastFrameTime += this.interval;
    return

    const timeToNext = this.interval - excess;
    // execute the rest of the frames
    for (let i = 1; i < numFrames; i++) {
      setTimeout(() => {

      this.executeFrame(false)
      this.lastFrameTime += this.interval;
      }, timeToNext * i / numFrames)
    }

    if(numFrames > 1) {
      console.log(`[sampler] skipped frames: ${numFrames-1}`)
    }
  }

  executeFrame(draw = true) {
    // clear BG
    if(draw) {
      this.contextMod.fillRect(0, 0, this.canvasMod.width, this.canvasMod.height)
    }

    this.tracks.forEach(track => {
        track.executeFrame(this.contextMod, false, draw)
    })

    if (draw && this.customDraw) {
      this.customDraw()
    }
  }

  loop(clearColor = "rgba(0, 0, 0, 1.0)", customDraw) {
    this.contextMod.fillStyle = clearColor

    if (customDraw) {
      this.customDraw = customDraw.bind(this)
    }

    window.requestAnimationFrame(this.frame);
  }
}