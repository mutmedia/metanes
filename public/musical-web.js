
import './lib/jsnes.js'
import downloadObjectAsJson from './lib/downloadObjectAsJson.js'

import NESsampler from './NESsampler/NESsampler.js'
import NEStrack from './NESsampler/NEStrack.js'
import DevTools from './NESsampler/DevTools.js'

import KEYS from './NESsampler/inputMaps/onePlayerDefault.js'

const KEYS_R = {
  32: [1, jsnes.Controller.BUTTON_A], // space
  38: [1, jsnes.Controller.BUTTON_A], // Up
  40: [1, jsnes.Controller.BUTTON_A], // Down
  37: [1, jsnes.Controller.BUTTON_LEFT], // Left
  39: [1, jsnes.Controller.BUTTON_RIGHT], // Right

  73: [1, jsnes.Controller.BUTTON_A], // I
  75: [1, jsnes.Controller.BUTTON_A], // K
  74: [1, jsnes.Controller.BUTTON_LEFT], // J
  76: [1, jsnes.Controller.BUTTON_RIGHT], // L
}
const KEYS_L = {
  32: [1, jsnes.Controller.BUTTON_A], // space
  38: [1, jsnes.Controller.BUTTON_A], // Up
  40: [1, jsnes.Controller.BUTTON_A], // Down
  37: [1, jsnes.Controller.BUTTON_LEFT], // Left
  39: [1, jsnes.Controller.BUTTON_RIGHT], // Right

  87: [1, jsnes.Controller.BUTTON_A], // W
  83: [1, jsnes.Controller.BUTTON_A], // S
  65: [1, jsnes.Controller.BUTTON_LEFT], // A
  68: [1, jsnes.Controller.BUTTON_RIGHT], // D
}

const ROMS = {
  breaking: 'breaking.nes'
}

const SAVE_STATES = {
  breakerGameplay: "breaker-gameplay.json"
}

function bgr2rgb(color) {
  return ((0xff0000 & color) >> 0x10) | (0xff00 & color) | ((0xff & color) << 0x10)
}

window.currentMemTest = 'eq'
window.checkMem = false
window.filterMem = null


function lerp(a, b, t) {
  t = Math.min(Math.max(t, 0), 1)
  return a + (b - a) * t
}

// window.filterMem = window.localStorage.getItem('filtermem') || null
// if(window.filterMem != null) {
  // window.filterMem = JSON.parse(window.filterMem)
// }

let focusedTrack
async function main(roms, savestates) {
  // Create AudioContext
  const WAContext = window.AudioContext || window.webkitAudioContext
  const context = new WAContext()

  // load patcher
  const rawPatcher = await fetch("./rnbo/granulator.export.json")
  const patcher = await rawPatcher.json()

  const gainNode = context.createGain()
  gainNode.gain.value = 2
  gainNode.connect(context.destination)

  // load samples
  const rawDependencies = await fetch("./rnbo/dependencies.json");
  const dependencies = await rawDependencies.json();
  const rawReverb = await fetch("./rnbo/rnbo.platereverb.json");
  const reverbPatcher = await rawReverb.json();
  const reverbDevice = await RNBO.createDevice({ context, patcher: reverbPatcher });
  reverbDevice.node.connect(gainNode)

  const decay = reverbDevice.parametersById.get("decay")
  // decay.value = 100

  async function CreateDevice() {
    const device = await RNBO.createDevice({ context, patcher })
    const results = await device.loadDataBufferDependencies(dependencies)
    // This connects the device to audio output, but you may still need to call context.resume()
    // from a user-initiated function.

    device.node.connect(reverbDevice.node)
    // device.node.connect(context.destination)

    // get device parameters
    const test = device.parametersById.get("test")
    // test.value = 10

    // mud
    const mud = {}
    mud.play = device.parametersById.get("mudPlay")
    mud.playDuration = device.parametersById.get("mudPlayDuration")
    mud.gain = device.parametersById.get("mudGain")
    mud.start = device.parametersById.get("mudStart")
    mud.length = device.parametersById.get("mudLength")
    mud.rate = device.parametersById.get("mudRate")
    mud.pan = device.parametersById.get("mudPan")
    mud.play = device.parametersById.get("mudPlay")

    // bubbles
    const bubbles = {}
    bubbles.play = device.parametersById.get("bubblesPlay")
    bubbles.playDuration = device.parametersById.get("bubblesPlayDuration")
    bubbles.gain = device.parametersById.get("bubblesGain")
    bubbles.start = device.parametersById.get("bubblesStart")
    bubbles.length = device.parametersById.get("bubblesLength")
    bubbles.rate = device.parametersById.get("bubblesRate")
    bubbles.pan = device.parametersById.get("bubblesPan")
    bubbles.play = device.parametersById.get("bubblesPlay")

    // pebbles
    const pebbles = {}
    pebbles.play = device.parametersById.get("pebblesPlay")
    pebbles.playDuration = device.parametersById.get("pebblesPlayDuration")
    pebbles.gain = device.parametersById.get("pebblesGain")
    pebbles.start = device.parametersById.get("pebblesStart")
    pebbles.length = device.parametersById.get("pebblesLength")
    pebbles.rate = device.parametersById.get("pebblesRate")
    pebbles.pan = device.parametersById.get("pebblesPan")

    for (let thing of [pebbles, bubbles, mud]) {
      thing.play.value = 1
      thing.playDuration.value = 100
      thing.gain.value = 200
      thing.start.value = 0
      thing.length.value = 40000
      thing.rate.value = 1
    }

    pebbles.gain.value = 40
    device.mud = mud
    device.pebbles = pebbles
    device.bubbles = bubbles

    return device
  }


  // This connects the device to audio output, but you may still need to call context.resume()
  // from a user-initiated function.
  console.log("started")
  const SCREEN_WIDTH = 800
  const SCREEN_HEIGHT = 800

  async function SimpleGame(id, keys, sample) {
    const $container = document.getElementById(id)
    const $dumpling = document.getElementById(`dump-${id}`)
    const containerId = $container.id

    const d = await CreateDevice()

    const sampler = new NESsampler(SCREEN_WIDTH, SCREEN_HEIGHT, id)

    const track = new NEStrack(
      { // parameters
        name: id,
        x: 0,
        y: 0,
        width: SCREEN_WIDTH,
        height: SCREEN_HEIGHT,
        hasControl: true,
        controller: keys,
      },
      function update() {
        const posX = this.nes.cpu.load(0x012F)
        const posY = this.nes.cpu.load(0x012E)
        this.nes.cpu.write(0x008C, 1)
        // this.nes.cpu.write(0x0100, 5)
        // this.nes.cpu.write(0x0101, 16)
        const velX = posX - this.state.posX
        const velY = posY - this.state.posY

        // i don't really know what this value does, but it gives me hit info
        const dir = this.nes.cpu.load(0x0033)

        if (
          this.state.dir !== dir
        ) {
          d.pebbles.play.value = Math.random() * 0.01 + 0
          d.mud.play.value = Math.random() * 0.01 + 0
          d.bubbles.play.value = Math.random() * 0.01 + 0
        }

        this.state.posX = posX
        this.state.posY = posY
        if(velX !== 0) {
          this.state.velX = velX
        }
        if(velY !== 0) {
          this.state.velY = velY
        }
        this.state.dir = dir
      },
      function shader(buffer, x, y) { // shader
        let ri = (y) * 256 + (x)
        return 0xff000000 | buffer[ri]
      },
      sample
    )

    $dumpling.addEventListener("drag-start", () => {
      console.log("HOLD")
      track.pause()
    })

    $dumpling.addEventListener("drag-end", () => {
      track.unpause()
      console.log("UNHOLD")
    })

    $dumpling.addEventListener("drag", () => {
      console.log("dragging")
      const r = $dumpling.getBoundingClientRect()
      const x = r.left / window.innerWidth
      const y = r.top / window.innerHeight

      d.bubbles.start.value = lerp(0, 400000, x*x + y*y)
      d.mud.start.value = lerp(0, 400000, x*x + y*y)
      d.pebbles.start.value = lerp(0, 400000, x*x + y*y)

      d.bubbles.pan.value = lerp(0, 1, x)
      d.mud.pan.value = lerp(0, 1, x)
      d.pebbles.pan.value = lerp(0, 1, x)
    })

    $dumpling.addEventListener("scale", () => {
      const r = $dumpling.getBoundingClientRect()
      const area = r.height * r.width
      const areaIndex = area / (window.innerWidth * window.innerHeight)
      const ratio = r.width / r.height

      // console.log(areaIndex)
      // console.log(ratio)

      d.bubbles.rate.value = lerp(20, 0.3, areaIndex)
      d.mud.rate.value = lerp(20, 0.3, areaIndex)

      d.mud.gain.value = lerp(0, 120, ratio) * 1.5
      d.bubbles.gain.value = lerp(0, 100, 1/ratio) 1.5
    })

    $dumpling.addEventListener("scale-end", () => {
        const r = $dumpling.getBoundingClientRect()
        const area = r.height * r.width
        const areaIndex = area / (window.innerWidth * window.innerHeight)
        const ratio = r.width / r.height

        // console.log(areaIndex)
        // console.log(ratio)

        const maxDuration = 1500
        d.bubbles.playDuration.value = lerp(20, maxDuration, areaIndex)
        d.mud.playDuration.value = lerp(20, maxDuration, areaIndex)

        d.bubbles.length.value = lerp(20, maxDuration, areaIndex) * 441
        d.mud.length.value = lerp(20, maxDuration, areaIndex) * 960
    })

    track.state = {}

    sampler.tracks.push(track)
    // track.pause()

    $dumpling.show()
    return sampler
  }

  // console.log()
  const breakerSample = {
    rom: roms.breaking,
    initialState: savestates.breakerGameplay,
    restartTrigger: function() {
        const v = this.nes.cpu.load(0x000D)
        return v < 3
    },
  }

  const right = await SimpleGame("right", KEYS_R, breakerSample)
  const left = await SimpleGame("left", KEYS_L, breakerSample)

  right.loop()
  left.loop()
  focusedTrack = right.tracks[0]
  DevTools(() => focusedTrack)
}

async function Setup(roms, savestates, callback) {


  const downloadedRoms = {}
  const downloadedSaves = {}
  for(const romKey of Object.keys(roms)) {
    const romName = roms[romKey]
    let xhr = new XMLHttpRequest();
    xhr.open('GET', `./raw_games/${romName}`);
    xhr.overrideMimeType("text/plain; charset=x-user-defined");
    xhr.send();

    xhr.onload = function() {
      if (xhr.status != 200) { // analyze HTTP status of the response
        console.error(`Error ${xhr.status}: ${xhr.statusText}`); // e.g. 404: Not Found
      } else { // show the result
        // alert(`Done, got ${xhr.response.length} bytes`); // response is the server response
        downloadedRoms[romKey] = xhr.responseText
        onLoad()
      }
    }
  }

  for(const saveKey of Object.keys(savestates)) {
    const saveStateName = savestates[saveKey]
    let xhr2 = new XMLHttpRequest();
    xhr2.open('GET', `./save_states/${saveStateName}`);
    // xhr2.overrideMimeType("text/plain; charset=x-user-defined");
    xhr2.send();
    xhr2.onload = function() {
      if (xhr2.status != 200) { // analyze HTTP status of the response
        console.error(`Error ${xhr2.status}: ${xhr2.statusText}`); // e.g. 404: Not Found
      } else { // show the result
        const saveState = JSON.stringify(JSON.parse(xhr2.responseText))
        downloadedSaves[saveKey] = saveState
        onLoad()
      }
    }
  }

  async function onLoad() {
    if(Object.keys(downloadedSaves).length < Object.keys(savestates).length) return;
    if(Object.keys(downloadedRoms).length < Object.keys(roms).length) return;
    await main(downloadedRoms, downloadedSaves)
    $loadDump.hide();
  }
}

const $start = document.getElementById("start")
const $startDump = document.getElementById("start-dump")
const $loadDump = document.getElementById("load-dump")
$start.addEventListener("click", () => {
  Setup(ROMS, SAVE_STATES)
  $startDump.hide()
  $loadDump.show()
})