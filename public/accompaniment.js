import './lib/jsnes.js'
import downloadObjectAsJson from './lib/downloadObjectAsJson.js'

const romName = 'jctennis.nes'
// const saveStateName = null
const saveStateName = 'jctennis-main.json'
// const saveStateName = 'game1-close-to-automario.json'
const trackRestartEvent = new Event('track.restarted');
// const saveStateName = 'game1-pipe.json'

let FRAME = 1
const PLAYER_1 = 1
const PLAYER_2 = 2
const VIDEO_FPS = 24
const GAME_FPS = 24

const followYelloPlayer = true
let dontUpdate = false
let yellowDudeTrack = null
let yellowDudeHitting = false
let yellowDudeHit = null

window.currentMemTest = 'eq'
window.checkMem = false
window.filterMem = null
// window.filterMem = window.localStorage.getItem('filtermem') || null
// if(window.filterMem != null) {
  // window.filterMem = JSON.parse(window.filterMem)
// }

let prevMem = 0;
function Compare(a, b) {
  const op = window.currentMemTest
  switch(op) {
    case 'leq':
      return a <= b
      break;
    case 'geq':
      return a >= b
      break;
    case 'gt':
      return a > b
      break;
    case 'lt':
      return a < b
      break;
    case 'neq':
      return a != b
      break;
    case 'eq':
      return a == b
      break;
  }

  console.error(`${op} does not exist`);
  return true
}

// Stuff to get the position of the cpu player
const CPUxAddress = 0x2e
const CPUxMinVal = 1
const CPUxMaxVal = 254
const CPUxMinPos = 0.15875
const CPUxMaxPos = 0.8475

function map_range(value, low1, high1, low2, high2) {
    return low2 + (high2 - low2) * (value - low1) / (high1 - low1);
}

function convertMemToPos(val) {
  return map_range(val, CPUxMinVal, CPUxMaxVal, CPUxMinPos, CPUxMaxPos)
}

function convertGameToVideoFrame(frame) {
  const gameTimeElapsed = frame / GAME_FPS
  return Math.floor(VIDEO_FPS * gameTimeElapsed % 2031)
}

const main = (mainROM, downloadedSaveState) => {
  const video = document.getElementById("backgroundVideo")
  // video.stop();
  video.play();
  const KEYS = {
    // 88: [PLAYER_1, jsnes.Controller.BUTTON_A], // X
    // 89: [PLAYER_1, jsnes.Controller.BUTTON_B], // Y (Central European keyboard)
    // 90: [PLAYER_1, jsnes.Controller.BUTTON_B], // Z
    // 17: [PLAYER_1, jsnes.Controller.BUTTON_SELECT], // Right Ctrl
    // 8: [PLAYER_1, jsnes.Controller.BUTTON_SELECT], // Backspace
    // 13: [PLAYER_1, jsnes.Controller.BUTTON_START], // Enter

    // 38: [PLAYER_1, jsnes.Controller.BUTTON_UP], // Up
    // 40: [PLAYER_1, jsnes.Controller.BUTTON_DOWN], // Down
    // 37: [PLAYER_1, jsnes.Controller.BUTTON_LEFT], // Left
    // 39: [PLAYER_1, jsnes.Controller.BUTTON_RIGHT], // Right
    // // 190: [PLAYER_1, jsnes.Controller.BUTTON_A], // X
    // 191: [PLAYER_1, jsnes.Controller.BUTTON_B], // Z
    // 32: [PLAYER_1, jsnes.Controller.BUTTON_B], // Z

    // 79: [PLAYER_1, jsnes.Controller.BUTTON_A], // O
    // 80: [PLAYER_1, jsnes.Controller.BUTTON_B], // P

    38: [PLAYER_2, jsnes.Controller.BUTTON_UP], // Up
    40: [PLAYER_2, jsnes.Controller.BUTTON_DOWN], // Down
    37: [PLAYER_2, jsnes.Controller.BUTTON_LEFT], // Left
    39: [PLAYER_2, jsnes.Controller.BUTTON_RIGHT], // Right
    // 87: [PLAYER_2, jsnes.Controller.BUTTON_UP], // W
    // 83: [PLAYER_2, jsnes.Controller.BUTTON_DOWN], // S
    // 65: [PLAYER_2, jsnes.Controller.BUTTON_LEFT], // A
    // 68: [PLAYER_2, jsnes.Controller.BUTTON_RIGHT], // D
    13: [PLAYER_2, jsnes.Controller.BUTTON_START], // Enter
    // 67: [PLAYER_2, jsnes.Controller.BUTTON_A], // C
    // 86: [PLAYER_2, jsnes.Controller.BUTTON_B], // V
    88: [PLAYER_2, jsnes.Controller.BUTTON_A], // X
    89: [PLAYER_2, jsnes.Controller.BUTTON_B], // Z
  }

  const SCREEN_WIDTH = 800
  const SCREEN_HEIGHT = 800
  class NESampler {
    // public fields
    contextMod = null

    canvasMod = null

    constructor(width, height, canvasElementId = 'screen_mod') {
      this.canvasMod = document.getElementById('screen_mod')
      this.contextMod = this.canvasMod.getContext('2d')
      this.canvasMod.style.display = 'block'

      document.getElementById('loading').style.display = 'none'
      document.getElementById('videoGameContainer').style.display = 'block'

      this.contextMod.fillStyle = 'blue'
      // set alpha to opaque
      this.contextMod.fillRect(0, 0, width, height)
    }

    createTrack() {
      
    }

    Loop() {
      // window.requestAnimationFrame(loop);

      // TODO: make this be a consistent framerate?
      window.setTimeout(() => this.Loop(), 1000 / GAME_FPS)

      tracks.forEach(track => {

          this.contextMod.clearRect(0, 0, this.canvasMod.width, this.canvasMod.height)
          track.executeFrame()
      })
    }
  }

  const nesampler = new NESampler()
  const contextMod = nesampler.contextMod
  const canvasMod = nesampler.canvasMod

  let tracks = []
  const NES_WIDTH = 256
  const NES_HEIGHT = 240
  function CreateTrack (settings, update, shader, sample) {
    // #region Canvas bullshit
    const canvasTrack = document.createElement('canvas')
    canvasTrack.width = NES_WIDTH
    canvasTrack.height = NES_HEIGHT
    const contextTrack = canvasTrack.getContext('2d')
    const imageDataTrack = contextTrack.getImageData(0, 0, NES_WIDTH, NES_HEIGHT)
    contextTrack.fillStyle = 'black'
    // set alpha to opaque
    contextTrack.fillRect(0, 0, 256, 240)

    // buffer to write on next animation frame
    let bufTrack = new ArrayBuffer(imageDataTrack.data.length)
    let buf8Track = new Uint8ClampedArray(bufTrack)
    let buf32Track = new Uint32Array(bufTrack)

    // Set alpha
    for (let i = 0; i < buf32Track.length; ++i) {
      buf32Track[i] = 0xff000000
    }
    // #endregion

    let track = canvasTrack
    track.prevMem = []
    track.drawSettings = settings
    track.draw = () => {
      const context = contextMod
      const scaleX = track.drawSettings.width / NES_WIDTH
      const scaleY = track.drawSettings.height / NES_HEIGHT

      imageDataTrack.data.set(buf8Track)
      contextTrack.putImageData(imageDataTrack, 0, 0)

      context.save()
      context.scale(scaleX, scaleY)
      context.drawImage(canvasTrack, track.drawSettings.x, track.drawSettings.y)
      context.restore()
    }

    track.update = (force) => {
      if(force || track.isPlaying) {
        track.nes.frame()
      }
      update(track)
    }

    track.executeFrame = (force) => {

      track.update(force)
      track.draw()

      if(track.restartTrigger && track.restartTrigger()) {
          track.dispatchEvent(trackRestartEvent)
          track.restart()
      }
      track.prevMem = track.nes.cpu.mem.map(a => a)
    }

    track.loaded = false
    track.frame = 0

    let nes = new jsnes.NES({
      onFrame: (buffer) => {
        for (let y = 0; y < NES_HEIGHT; ++y) {
          for (let x = 0; x < NES_WIDTH; ++x) {
            let i = (y * 256 + x)
            buf32Track[i] = shader(buffer, x, y)
          }
        }
        
        // var colors = buf32Track.reduce((p, c) => {
        //   var k = c.toString(16)
        //   if(k in p) {
        //     p[k]++
        //   } else {
        //     p[k] = 1
        //   }
        //   return p;
        // }, {});
        // console.log(colors)

        track.frame++
      },
      onAudioSample: function (left, right) {
      }
    })

    track.nes = nes

    if (settings.hasControl) {
      document.addEventListener('keyup', (e) => {
        const key = KEYS[e.keyCode]
        if (key) {
          nes.buttonUp(key[0], key[1])
          // e.preventDefault()
        }
      })
      document.addEventListener('keydown', (e) => {
        const key = KEYS[e.keyCode]
        if (key) {
          nes.buttonDown(key[0], key[1])
          // e.preventDefault()
        }
      })
    }

    nes.loadROM(mainROM)
    nes.frame()
    console.log('loaded rom')
    if (sample) {
      console.log("loading track", sample)

      if(sample.initialState) {
        track.nes.fromJSON(JSON.parse(sample.initialState))
      }

      sample.initialState = JSON.stringify(track.nes.toJSON())

      if(sample.restartTrigger) {
        track.restartTrigger = () => sample.restartTrigger(track)
        track.onRestart
      }

      if(sample.onRestart) {
        track.onRestart = () => sample.onRestart(track)
      }

      if (sample.loopTime) {
        setInterval(() => {
          track.nes.fromJSON(JSON.parse(sample.initialState))
        }, sample.loopTime)
      }

      console.log('loaded sample')
    }

    track.restart = function() {
      if(track.onRestart) {
        track.onRestart()
      }
      if(sample) {
        track.nes.fromJSON(JSON.parse(sample.initialState))
      } else {
        track.nes.reset()
      }
    }

    track.isPlaying = true

    track.unpause = function() {
      track.isPlaying = true
    }

    track.pause = function() {
      track.isPlaying = false
    }

    track.togglePause = function() {
      track.isPlaying = !track.isPlaying
    }

    track.loaded = true

    tracks.push(track)

    return track
  }

  const FRAMES_TO_SAVE = 30
  const savedFrames = new Array(FRAMES_TO_SAVE)
  const SaveFrame = (track) => {
    savedFrames[track.frame % FRAMES_TO_SAVE] = JSON.stringify(track.nes.toJSON())
  }

  const GetFrame = (currentFrame, delta) => {
    return savedFrames[(currentFrame - delta + FRAMES_TO_SAVE) % FRAMES_TO_SAVE]
  }

  function DoMemoryCheck(track) {
    const mem = track.nes.cpu.mem
    if(window.filterMem == null) {
      window.filterMem = mem.map((m, i) => { 
        return [m, null]
      })
    } else {
      window.filterMem = window.filterMem.map((m, i) => {
        if(m == null) return null;
        if(Compare(m[0], mem[i])) {
          return [mem[i], mem[i] - m[0]]
        } else {
          return null
        }
      })

      const mappedMem = window.filterMem.reduce((p, c, i) => {
        if(c != null) {
          p[i.toString(16)] = c
        }
        return p
      }, {})

      console.log(mappedMem)
    }
  }

  const mainTrack = CreateTrack(
    {
      x: 0,
      y: 0,
      width: SCREEN_WIDTH,
      height: SCREEN_HEIGHT,
      hasControl: true
    },
    (track) => {
      if(window.checkMem) {
        DoMemoryCheck(track)
      }

      // console.log(video.currentTime)
      if(followYelloPlayer) {
        const videoFrame = Math.floor(video.currentTime * 24) + 2
        const yellowDudeCurrentPosition = yellowDudeTrack[videoFrame].x
        // if(yellowDudeHit === null) {
          // yellowDudeHit = new Array(yellowDudeTrack.length)
        // } else if(videoFrame == 2) {
          // downloadObjectAsJson(yellowDudeHit)
        // }
        // yellowDudeHit[videoFrame] = yellowDudeHitting
        const hit = yellowDudeHit[videoFrame] || false
        if(hit) {
          track.nes.buttonDown(PLAYER_1, jsnes.Controller.BUTTON_A)
        } else {
          track.nes.buttonUp(PLAYER_1, jsnes.Controller.BUTTON_A)
        }

        // nes.buttonDown(PLAYER_1, key[1])
        const cpuCurrentPos = convertMemToPos(track.nes.cpu.mem[CPUxAddress]) 
        const error = cpuCurrentPos - yellowDudeCurrentPosition
        track.nes.buttonUp(PLAYER_1, jsnes.Controller.BUTTON_LEFT)
        track.nes.buttonUp(PLAYER_1, jsnes.Controller.BUTTON_RIGHT)
        if(Math.abs(error) > 0.0001) {
          let dir = jsnes.Controller.BUTTON_RIGHT
          if(error > 0) {
            dir = jsnes.Controller.BUTTON_LEFT
          }
          track.nes.buttonDown(PLAYER_1, dir)
        }
      }
      if(track.prevMem != null && track.prevMem[0x55e] != track.nes.cpu.mem[0x55e] && track.nes.cpu.mem[0x55e] === 2) {
        video.play()
      }
      // , track.nes.cpu.mem[0xa5])
      // console.log(yellowDudeCurrentPosition, cpuCurrentPos, error)
    },
    (buffer, x, y) => { // shader
      let xd = x
      let yd = y
      let ri = (yd) * 256 + (xd)
      const c = buffer[ri]
      const alphaColors = [0x008c03, 0xFF4000]
      // const alphaColors = [0x008c03, 0xFF4000]
      const alpha = parseInt(`0x${Math.floor(Math.max(0, ((y-30)*255.0/100.0))).toString(16)}000000`)
      // console.log(alpha.toString(16))

      if(alphaColors.includes(c)) {
        return 0x00000000 | c
      }
      if(y < 70) return alpha | c
      return 0xff000000 | c
    }
    ,{
      initialState: downloadedSaveState && JSON.stringify(downloadedSaveState) || null,
      restartTrigger: function(track) {
        if(track.prevMem == null) return false
        return track.prevMem[0x55e] != track.nes.cpu.mem[0x55e] && track.nes.cpu.mem[0x55e] === 254
      },
      onRestart: function(track) {
        video.pause()
        video.currentTime = 0
      }
    }
  )

  nesampler.Loop()

  let saveState
  let palette = 0;
  let fakeDeath = false;

  function randomizeMemory() {
    mainTrack.nes.cpu.write(Math.floor(Math.random() * 0x87F4), Math.floor(Math.random() * 256)) // normal
  }
  
  document.addEventListener('keydown', (e) => {
    if(e.keyCode == 32) {
      yellowDudeHitting = true;
        // window.checkMem = true;
   }
  })

  document.addEventListener('keyup', (e) => {
    switch (e.keyCode) {
      case 49: // 1 Check memory
        DoMemoryCheck(mainTrack)
        break
      case 50: // 2 Save memory
        window.localStorage.setItem(
          'filtermem', 
          JSON.stringify(window.filterMem))
        break
      case 51: // 3 Clear memory
        window.localStorage.setItem(
          'filtermem', 
          null)
        window.filterMem = null
        break
      case 52: // 4
        if(saveState !== null) {
          mainTrack.nes.fromJSON(JSON.parse(saveState))
        }
        break
      case 53: // 5
        saveState = JSON.stringify(mainTrack.nes.toJSON());
        break
      case 54: // 6
        downloadObjectAsJson(mainTrack.nes.toJSON())
        break
      case 55: // 7
        mainTrack.reset()
        break
      case 32: // space
        yellowDudeHitting = false;
        // window.checkMem = false;
      case 67: // C
        break
      case 48: // 0
        window.Compare
        if(window.currentMemTest === 'eq') {
          window.currentMemTest = 'neq'
        } else {
          window.currentMemTest = 'eq'
        }
        console.log("CURRENT OP", window.currentMemTest)
        
        break;
      case 79: // O
        mainTrack.executeFrame(true)
        break
      case 80: // P
        mainTrack.togglePause()
        break
      case 82: // R
        // mainTrack.nes.fromJSON(JSON.parse(GetFrame(mainTrack.frame, 30)))
        mainTrack.restart();
        // CreateKillTrack()
        break
      }
    })
  }

document.getElementById("playButton").onclick = function() {
  Setup();
}

function Setup() {
  let xhr = new XMLHttpRequest();
  xhr.open('GET', `./raw_games/${romName}`);
  xhr.overrideMimeType("text/plain; charset=x-user-defined");
  xhr.send();

  // 4. This will be called after the response is received
  xhr.onload = function() {
    if (xhr.status != 200) { // analyze HTTP status of the response
      console.error(`Error ${xhr.status}: ${xhr.statusText}`); // e.g. 404: Not Found
    } else { // show the result
      // alert(`Done, got ${xhr.response.length} bytes`); // response is the server response
      var loadedROM = xhr.responseText
      if(saveStateName) {
        let xhr2 = new XMLHttpRequest();
        xhr2.open('GET', `./save_states/${saveStateName}`);
        // xhr2.overrideMimeType("text/plain; charset=x-user-defined");
        xhr2.send();
        xhr2.onload = function() {
          if (xhr2.status != 200) { // analyze HTTP status of the response
            console.error(`Error ${xhr2.status}: ${xhr2.statusText}`); // e.g. 404: Not Found
          } else { // show the result
            const saveState = JSON.parse(xhr2.responseText)
            let xhr3 = new XMLHttpRequest();
            xhr3.overrideMimeType("text/plain; charset=x-user-defined");
            xhr3.open('GET', `./stuff/tracks/yellowDudeTrack.csv`);
            xhr3.send();
            xhr3.onload = function() {
              if (xhr3.status != 200) { // analyze HTTP status of the response
                console.error(`Error ${xhr3.status}: ${xhr3.statusText}`); // e.g. 404: Not Found
              } else { // show the result
                yellowDudeTrack = xhr3.responseText.split('\n').map(t => {
                  const split = t.split(',')
                  return {
                    frame: split[0],
                    x: split[1]/720,
                    y: split[2]/720,
                  }
                })
                let xhr4 = new XMLHttpRequest();
                xhr4.open('GET', `./stuff/tracks/hittingball.json`);
                xhr4.send();
                xhr4.onload = function() {
                  if (xhr4.status != 200) { // analyze HTTP status of the response
                    console.error(`Error ${xhr4.status}: ${xhr4.statusText}`); // e.g. 404: Not Found
                  } else { // show the result
                    console.log(xhr4.responseText)
                    yellowDudeHit = JSON.parse(xhr4.responseText)
                    console.log(yellowDudeTrack)
                    main(loadedROM, saveState)
                  }
                }
              }
            }
          }
        }
      } else {
        main(loadedROM)
      }
    }
  }

  // function loadStuffAndExecute() {
  //   const options = {
  //       method: 'GET',
  //       headers:{'content-type': 'text/plain; charset=x-user-defined'},
  //       mode: 'no-cors'
  //     };
  //   fetch(`./raw_games/${romName}`, options)
  //     .then(res => {
  //       if(res.ok) {
  //         return res.text();
  //       } else {
  //         throw Error(`Request rejected with status ${res.status}`);
  //       }
  //     })
  //     .then(loadedROM => {
  //         main(loadedROM)
  //         // fetch (`./save_states/${saveStateName}`)
  //           // .then(response => response.json())
  //       // .then(j => {
  //         // downloadedSaveState = j
  //         // main(loadedROM)
  //       // })
  //     })
  //     .catch(console.error)
  // }

  // // loadStuffAndExecute()

}