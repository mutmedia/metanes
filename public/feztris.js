import NESsampler from './NESsampler/NESsampler.js'
import NEStrack from './NESsampler/NEStrack.js'
import DevTools from './NESsampler/DevTools.js'
import KEYS from './NESsampler/inputMaps/onePlayerDefault.js'

import loadRomsAndSaves from './lib/loadRomsAndSaves.js'

import * as THREE from './lib/three.js/three.module.js';
import { OrbitControls } from './lib/three.js/OrbitControls.js';
const kPalette = {
    lightBlue:    0x6ac0bd,
    whiteblue:    0x9ee7d7,
    darkBlue:     0x5889a2,
    blackPurple:  0x462c4b,
    darkBrown:    0x724254,
    lightBrown:   0xc18c72,
    whiteYellow:  0xfcebb6,
    lime:         0xa9f05f,
    green:        0x5fad67,
    darkGreyn:    0x4e5e5e,
}

const kTetrisPalette = []
const kPaletteMap = {
  0:        "0x462c4b",
  2232028:  "0x9ee7d7",
  3976191:  "0x5889a2",
  5395026:  "0x4e5e5e",
  11599871: "0x462c4b",
  12327168: "0xc18c72",
  16777215: "0x9ee7d7",
}
window.palette = kPaletteMap
const scene = new THREE.Scene();
// const camera = new THREE.PerspectiveCamera(75, window.innerWidth / window.innerHeight, 0.1, 1000);
const camera = new THREE.OrthographicCamera( -400, 400, 400, -400, 0.1, 1000 );

const $render = document.getElementById("render")
const $renderCanvas = $render.querySelector(".game-screen")

const renderer = new THREE.WebGLRenderer({canvas: $renderCanvas});
// renderer.setClearColor(kPalette.blackPurple)
renderer.setClearColor(kPalette.lime)

const controls = new OrbitControls( camera, renderer.domElement );

const boxGeometry = new THREE.BoxGeometry(1, 1, 1);

const planeGeometry = new THREE.PlaneGeometry(10000, 10000);
const groundMaterial = new THREE.MeshStandardMaterial({ color: kPalette.whiteblue, side: THREE.DoubleSide});
const ground = new THREE.Mesh(planeGeometry, groundMaterial)
ground.rotation.x = Math.PI/2
ground.position.y = -5
ground.receiveShadow = true
// scene.add(ground)

const color = kPalette.whiteYellow;
const intensity = 1;
const light = new THREE.DirectionalLight(color, intensity);
light.position.set(10, 10, 10);
light.target.position.set(0, 0, 0);
window.shadow = light.shadow.camera
light.shadow.camera.left = -500
light.shadow.camera.right = 500
light.shadow.camera.top = 500
light.shadow.camera.bottom = -500
scene.add(light);
scene.add(light.target);

const wireVertices = new Float32Array( [
  1.0,  1.0,  1.0,
  1.0,  1.0,  0.0,

  1.0,  1.0,  1.0,
  1.0,  0.0,  1.0,

  1.0,  1.0,  1.0,
  0.0,  1.0,  1.0,

  0.0,  1.0,  1.0,
  0.0,  0.0,  1.0,

  0.0,  1.0,  1.0,
  0.0,  1.0,  0.0,

  1.0,  0.0,  1.0,
  0.0,  0.0,  1.0,

  1.0,  0.0,  1.0,
  1.0,  0.0,  0.0,

  1.0,  1.0,  0.0,
  1.0,  0.0,  0.0,

  1.0,  1.0,  0.0,
  0.0,  1.0,  0.0,
] );

const wireframe = new THREE.BufferGeometry();
wireframe.setAttribute( 'position', new THREE.BufferAttribute( wireVertices, 3 ) );
const wireMaterial = new THREE.MeshBasicMaterial({ color: kPalette.whiteYellow , transparent: true, opacity: 0.1});

const o = 0.1
const basicMaterial = new THREE.MeshBasicMaterial({ color: kPalette.green, side: THREE.DoubleSide })//, opacity: 1, transparent: o < 1});
const basicMaterialY = new THREE.MeshBasicMaterial({ color: kPalette.darkGreyn, side: THREE.DoubleSide, opacity: o, transparent: o < 1});
const basicMaterialZ = new THREE.MeshBasicMaterial({ color: kPalette.lightBrown, side: THREE.DoubleSide, opacity: o, transparent: o < 1});

const invisibleMaterial = new THREE.MeshBasicMaterial(
  { color: 0x000, transparent: true, opacity: 0.0});
const invalidMaterial = new THREE.MeshBasicMaterial(
  { color: kPalette.darkBrown, transparent: true, opacity: 0.1, side: THREE.DoubleSide});

function bgr2rgb(color) {
  return ((0xff0000 & color) >> 0x10) | (0xff00 & color) | ((0xff & color) << 0x10)
}

function CreateProj(size) {
  const proj = []
  for (let x = 0; x < size; x++) {
    const row = []
    proj.push(row)
    for (let y = 0; y < size; y++) {
      row.push(Math.random() * 2)
    }
  }
  return proj
}

let projs = {
  x: CreateProj(10),
  y: CreateProj(10),
  z: CreateProj(10)
}

function CreateWire(size) {
  for (let x = 0; x < size; x++) {
    for (let y = 0; y < size; y++) {
      for (let z = 0; z < size; z++) {
        const line = new THREE.LineSegments(wireframe, wireMaterial);
        scene.add(line);
        line.position.x = x - size / 2 - 0.5
        line.position.y = y - size / 2 - 0.5
        line.position.z = z - size / 2 - 0.5
      }
    }
  }

  return model
}

function CreateModel(size) {
  const model = []
  for (let x = 0; x < size; x++) {
    const row = []
    model.push(row)
    for (let y = 0; y < size; y++) {
      const cell = []
      row.push(cell)
      for (let z = 0; z < size; z++) {

        const cube = new THREE.Mesh(boxGeometry, [invisibleMaterial, invisibleMaterial,invisibleMaterial, invisibleMaterial,invisibleMaterial, invisibleMaterial ]);
        cube.castShadow = true
        cube.receiveShadow = true

        scene.add(cube);
        cube.visible = false
        cell.push(cube)
        const s = 0.9
        cube.scale.x = s
        cube.scale.y = s
        cube.scale.z = s

        cube.position.x = x - size/2 + 0.5
        cube.position.y = y - size/2 + 0.5
        cube.position.z = z - size/2 + 0.5
      }
    }
  }

  return model
}

function UpdateModel(model, xproj, yproj, zproj) {
  const size = model.length
  const cubed = size * size * size

  for (let x = 0; x < size; x++) {
    for (let y = 0; y < size; y++) {
      for (let z = 0; z < size; z++) {
        const vx = xproj[z][size-1-y]
        const vy = yproj[z][x]
        const vz = zproj[x][size-1-y]

        const sum =
          vx+vy+vz

        const cube = model[size-1-x][y][z]
        const s = 0.9
        cube.scale.x = s
        cube.scale.y = s
        cube.scale.z = s
        const goodMaterial = sum == 3
          ? basicMaterial
          : sum == 2
            ? basicMaterialZ
            : invalidMaterial
        cube.material = [
          vx > 0 ? goodMaterial : invisibleMaterial,
          vx > 0 ? goodMaterial : invisibleMaterial,
          vy > 0 ? goodMaterial : invisibleMaterial,
          vy > 0 ? goodMaterial : invisibleMaterial,
          vz > 0 ? goodMaterial : invisibleMaterial,
          vz > 0 ? goodMaterial : invisibleMaterial,
        ]

        cube.material = goodMaterial

        cube.visible = sum > 0
      }
    }
  }
}

const model = CreateModel(10)
// CreateWire(10)

const center = 3*model.length
camera.position.set(center, center, -center);
camera.zoom = 40
controls.update();

function animate() {
  requestAnimationFrame(animate);

  // required if controls.enableDamping or controls.autoRotate are set to true
  controls.update();

  renderer.render(scene, camera);
};

function UpdatePaletteWindow() {
const $palette = document.getElementById("palette-swap");

$palette.innerHTML = ""
kTetrisPalette.forEach(k => {
  const color = k.toString(16).padStart(6, "0")
const swapColor = (kPaletteMap[k] || color).toString(16).padStart(6, "0")
  const template = `
<div class="palette-color palette-original" style="background: #${color}">0x${color}</div>
<div class="palette-color palette-target" style="background: #${swapColor}"><input type="text"></div>`
  const $el = document.createElement("div")
  $el.classList.add("palette-row")
  $el.innerHTML = template
  $el.querySelector("input").addEventListener("input", (e) => {
    kPaletteMap[k] = `0x${e.target.value}`
  })
  $palette.appendChild($el)
})
}

function main(roms, states) {
// threes.js stuff
animate();

const SCREEN_WIDTH = 800
const SCREEN_HEIGHT = 800

const minoSize = 8
const xstart = 95
const xend = 175
const ystart = 126
const yend = 207

let anyGameOver = false
function ProjectionSampler(dumplingId, index) {
  const $dumpling = document.getElementById(dumplingId)
  const $container = $dumpling.querySelector(".game-screen")
  const containerId = $container.id
  const sampler = new NESsampler(SCREEN_WIDTH, SCREEN_HEIGHT, containerId)

  let prevY = 0
  let gameOver = false

  let others = {
    "x": [projs.y, projs.z],
    "y": [projs.x, projs.z],
    "z": [projs.y, projs.x],
  }[index]

  let texture = new THREE.CanvasTexture();

  const track = new NEStrack({
    name: dumplingId,
    x: 0,
    y: 0,
    width: SCREEN_WIDTH,
    height: SCREEN_HEIGHT,
    hasControl: true,
    controller: KEYS,
    },
    function update() {
      texture.needsUpdate = true
      // https://github.com/timothyng-164/nes-tetris-ai/blob/master/lua_scripts/playfield.lua
      const field_start_addr = 0x0400
      const field_end_addr = 0x04C7

      const num_rows = 20
      const num_cols = 10
      const field = [[]]
      let field_current_addr = field_start_addr
      gameOver = true
      for (let y = 0; y < num_rows; y ++) {
        const row = []
        field.push(row)
        for(let x = 0; x < num_cols; x++) {
          const value = this.nes.cpu.load(field_current_addr)
          const isFill = (value != 239)
          projs[index][x%10][y%10] = isFill ? 1 : 0
          row.push(isFill)
          if (!isFill) gameOver = false
          // -- initialize cells in field, 239 from memory is an empty cell
          field_current_addr = field_current_addr + 1
        }
      }

      if (gameOver) {
        anyGameOver = true
      }

      const currY = this.nes.cpu.load(0x0041)

      // new piece
      if(prevY > currY) {
        UpdateModel(model, projs.x, projs.y, projs.z)
      }

      prevY = currY
    },
    function shader(buffer, x, y) { // shader
      let ri = (y) * 256 + (x)
      const c0 = 0x00ffffff & bgr2rgb(buffer[ri])
      let c = c0
      if (!kTetrisPalette.includes(c)) {
        kTetrisPalette.push(c)
        UpdatePaletteWindow()
      }
      c = kPaletteMap[c] || c

      if (x > xstart && x < xend && y > ystart && y < yend) {
        // allow grid
        let max = 0
        if (x % 8 == 7) max = 1000
        if (y % 8 == 7) max = 1000
        const xx = x - xstart
        const yy = yend - y - 1
        let xpos = Math.floor(xx / minoSize)
        let ypos = 9-Math.floor(yy / minoSize)
        const v = projs[index][xpos][ypos]



        for(let i = 0; i < 10; i++) {
          const o0 = index!== "z"
                      ? others[0][xpos][i]
                      : others[0][ypos][i]
          const o1 = index !== "z"
                      ? others[1][i][ypos]
                      : others[1][xpos][i]
          max = Math.max(max, o0 + o1)
        }

        const sum = v + max

        if(sum === 3){
          c = kPalette.green
        }

        if (c0 == 0) {
          if(sum == 2){
            c = kPalette.lightBrown
          } else if (sum == 1){
            c = kPalette.darkBrown
          }
        }
      }

      return 0xff000000 | bgr2rgb(c)
    },
    {
      rom: roms.blocksfall,
      initialState: states[index],
      restartTrigger: function() {
        return anyGameOver
      },
      onRestart: function() {
        anyGameOver = false
        UpdateModel(model, projs.x, projs.y, projs.z)
      }
    })
    sampler.tracks.push(track)
    sampler.loop()


    // add the extra plane to the scene
    const plane = new THREE.PlaneGeometry(10, 10);
    texture = new THREE.CanvasTexture(track.context.canvas)
    texture.repeat.x = 0.315
    texture.repeat.y = 0.34
    texture.offset.x = 0.37
    texture.offset.y = 0.135
    const material = new THREE.MeshBasicMaterial({
      map: texture,
    });

    const mesh = new THREE.Mesh(plane, material)

    if(index === "z") {
      mesh.position.z += 6
      mesh.rotation.y = Math.PI

    }
    if(index === "y") {
      mesh.position.y -= 5
      mesh.rotation.x = -Math.PI/2
      mesh.rotateOnAxis(new THREE.Vector3(0, 0, 1), -Math.PI/2)
    }
    if(index === "x") {
      mesh.position.x += 5
      mesh.rotation.y = -Math.PI/2
    }

    scene.add(mesh)
  }

  const blocksfallSamplerX = ProjectionSampler("projectionx", "x")
  const blocksfallSamplerY = ProjectionSampler("projectiony", "y")
  const blocksfallSamplerZ = ProjectionSampler("projectionz", "z")

  // DevTools(() => blocksfallTrackX);
}

loadRomsAndSaves({
    blocksfall: 'blocksfall.nes'
  }, {
    x: 'blocksfall-gameplay.json',
    y: 'blocksfall-gameplay2.json',
    z: 'blocksfall-gameplay3.json'
  }, main)

