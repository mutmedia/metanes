import './lib/jsnes.js'
import downloadObjectAsJson from './lib/downloadObjectAsJson.js'

import NESsampler from './NESsampler/NESsampler.js'
import NEStrack from './NESsampler/NEStrack.js'

import controller from './NESsampler/inputMaps/onePlayerDefault.js'

import loadRomsAndSaves from './lib/loadRomsAndSaves.js'

const ROMS = {
  game1: 'game1.nes'
}

const SAVE_STATES = {
  // busStop: 'busstop.json'
  threemans: 'threemans.json'
}

const FRAME_RULE = 10

document.getElementById("playButton").onclick = function() {
  loadRomsAndSaves(ROMS, SAVE_STATES, main);
}

function main(roms, savestates) {
  const SCREEN_WIDTH = 800
  const SCREEN_HEIGHT = 800

  const game1Sampler = new NESsampler()
  const game1Rom = roms.game1

  let playerDied = false
  let currentControl = 1
  const createMarioTrack = (i) => {
    const firstPersonTrack = new NEStrack(
      {
        x: 0,
        y: 0,
        width: SCREEN_WIDTH,
        height: SCREEN_HEIGHT,
        hasControl: true,
        controller
      },
      function update() {
        console.log()
        if(this.nes.cpu.load(0x00E) === 0x06) {
          playerDied = true
        }
        // if(!this.createdTrack && this.frame === FRAME_RULE) {
        //   if(i < 3) {
        //     createMarioTrack(i+1)
        //     console.log("created a thing!")
        //   }
        //   this.createdTrack = true
        //   this.canControl = true
        // }
      },
      function shader(buffer, x, y) {
        const ri = y * 256 + x
        let c = bgr2rgb(buffer[ri])
        const ex = [0x00AB00, 0x0, 0x8179FF]
        if(i > 0) {
          if (ex.includes(c))
            return 0x0
        }
        if(i !== currentControl) {
          if(c === 0xBC1900)
            // c = 0x5E5E5E
            c = 0xFEEA5B
        }


        return 0xff000000 | bgr2rgb(c)

      },
      {
        rom: game1Rom,
        // initialState: savestates.busStop,
        initialState: savestates.threemans,
        restartTrigger: function() {
          return playerDied
        },
        onRestart: function() {
          setTimeout(() => playerDied = false, 0)
        }
      }
    )

    if(i!==currentControl)
      firstPersonTrack.removeControl()
    game1Sampler.tracks.push(firstPersonTrack)
    return firstPersonTrack
  }
  const mainTrack = createMarioTrack(0)
  createMarioTrack(1)
  // createMarioTrack(2)
    // game1Sampler.contextMod.globalCompositeOperation = 'overlay'
  game1Sampler.loop("rgba(0, 255, 0, 1.0)")

  setupSaveStates(mainTrack)
  document.addEventListener('keydown', (e) => {
    if(e.keyCode === 67) {
      game1Sampler.tracks[currentControl].removeControl()
      currentControl = (currentControl - 1 + 2) % 2
      game1Sampler.tracks[currentControl].giveControl()
    }
  })
}




// save state stuff
function setupSaveStates(track, keys = {
  save: 53, // 5
  load: 52, // 4
  download: 54 // 6
}) {
  document.addEventListener('keyup', (e) => {
    switch (e.keyCode) {
      case 52: // 4
        track.saveState()
        break
      case 53: // 5
        track.loadState()
        break
      case 54: // 6
        downloadObjectAsJson(track.nes.toJSON())
        break
      default:
        break
    }
  })
}

function bgr2rgb(color) {
  return ((0xff0000 & color) >> 0x10) | (0xff00 & color) | ((0xff & color) << 0x10)
}