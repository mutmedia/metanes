import './lib/jsnes.js'
import downloadObjectAsJson from './lib/downloadObjectAsJson.js'

import NESampler from './NESsampler/NESsampler.js'
import NEStrack from './NESsampler/NEStrack.js'

import KEYS from './NESsampler/inputMaps/onePlayerDefault.js'

const linkRomName = 'link.nes'
const linkRomName = 'link.nes'
const wiiRomName = 'wiigame.nes'
const saveStateName = null

window.currentMemTest = 'eq'
window.checkMem = false
window.filterMem = null
// window.filterMem = window.localStorage.getItem('filtermem') || null
// if(window.filterMem != null) {
  // window.filterMem = JSON.parse(window.filterMem)
// }

let prevMem = 0;
function Compare(a, b) {
  const op = window.currentMemTest
  switch(op) {
    case 'leq':
      return a <= b
      break;
    case 'geq':
      return a >= b
      break;
    case 'gt':
      return a > b
      break;
    case 'lt':
      return a < b
      break;
    case 'neq':
      return a != b
      break;
    case 'eq':
      return a == b
      break;
  }

  console.error(`${op} does not exist`);
  return true
}

const main = (linkRom, wiiRom) => {
  const SCREEN_WIDTH = 800
  const SCREEN_HEIGHT = 800

  // const nesSamplerLink = new NESampler()
  const nesSamplerWii = new NESampler()

  function DoMemoryCheck(track) {
    const mem = track.nes.cpu.mem
    if(window.filterMem == null) {
      window.filterMem = mem.map((m, i) => {
        return [m, null]
      })
    } else {
      window.filterMem = window.filterMem.map((m, i) => {
        if(m == null) return null;
        if(Compare(m[0], mem[i])) {
          return [mem[i], mem[i] - m[0]]
        } else {
          return null
        }
      })

      const mappedMem = window.filterMem.reduce((p, c, i) => {
        if(c != null) {
          p[i.toString(16)] = c
        }
        return p
      }, {})

      console.log(mappedMem)
    }
  }

  // const linkTrack = new NEStrack(
  //   nesSamplerLink.contextMod, // TODO: make it context independent?
  //   {
  //     x: 0,
  //     y: 0,
  //     width: SCREEN_WIDTH,
  //     height: SCREEN_HEIGHT,
  //     hasControl: true,
  //     controller: KEYS,
  //   },
  //   function update() {
  //     if(window.checkMem) {
  //       DoMemoryCheck(this)
  //     }
  //   },
  //   function shader(buffer, x, y) { // shader
  //     let ri = (y) * 256 + (x)
  //     const c = buffer[ri]
  //     return 0xff000000 | c
  //   }
  //   ,{
  //     rom: linkRom,
  //     restartTrigger: function() {
  //       return false
  //     },
  //     onRestart: function() {
  //     }
  //   }
  // )

  // let mainTrack = linkTrack
  // nesSamplerLink.tracks.push(linkTrack)

  const wiiTrack = new NEStrack(
    nesSamplerWii.contextMod,
    {
      x: 0,
      y: 0,
      width: SCREEN_WIDTH,
      height: SCREEN_HEIGHT,
      hasControl: true,
      controller: KEYS,
    },
    function update() {
    },
    function shader(buffer, x, y) { // shader
      let ri = (y) * 256 + (x)
      const c = buffer[ri]
      return 0xff000000 | c
    }
    ,{
      rom: wiiRom,
      restartTrigger: function() {
        return false
      },
      onRestart: function() {
      }
    }
  )
  nesSamplerWii.tracks.push(wiiTrack)

  nesSamplerWii.loop()
  // nesSamplerLink.loop()

  let saveState

  document.addEventListener('keyup', (e) => {
    switch (e.keyCode) {
      case 49: // 1 Check memory
        DoMemoryCheck(mainTrack)
        break
      case 50: // 2 Save memory
        window.localStorage.setItem(
          'filtermem',
          JSON.stringify(window.filterMem))
        break
      case 51: // 3 Clear memory
        window.localStorage.setItem(
          'filtermem',
          null)
        window.filterMem = null
        break
      case 52: // 4
        if(saveState !== null) {
          mainTrack.nes.fromJSON(JSON.parse(saveState))
        }
        break
      case 53: // 5
        saveState = JSON.stringify(mainTrack.nes.toJSON());
        break
      case 54: // 6
        downloadObjectAsJson(mainTrack.nes.toJSON())
        break
      case 55: // 7
        mainTrack.reset()
        break
      case 32: // space
        yellowDudeHitting = false;
        // window.checkMem = false;
      case 67: // C
        break
      case 48: // 0
        window.Compare
        if(window.currentMemTest === 'eq') {
          window.currentMemTest = 'neq'
        } else {
          window.currentMemTest = 'eq'
        }
        console.log("CURRENT OP", window.currentMemTest)

        break;
      case 79: // O
        mainTrack.executeFrame(true)
        break
      case 80: // P
        mainTrack.togglePause()
        break
      case 82: // R
        // mainTrack.nes.fromJSON(JSON.parse(GetFrame(mainTrack.frame, 30)))
        mainTrack.restart();
        // CreateKillTrack()
        break
      }
    })
  }

document.getElementById("playButton").onclick = function() {
  Setup();
}

function Setup() {

  let xhr = new XMLHttpRequest();
  xhr.open('GET', `./raw_games/${linkRomName}`);
  xhr.overrideMimeType("text/plain; charset=x-user-defined");
  xhr.send();

  xhr.onload = function() {
    if (xhr.status != 200) { // analyze HTTP status of the response
      console.error(`Error ${xhr.status}: ${xhr.statusText}`); // e.g. 404: Not Found
    } else { // show the result
      // alert(`Done, got ${xhr.response.length} bytes`); // response is the server response
      const linkRom = xhr.responseText
      let xhr2 = new XMLHttpRequest();
      xhr2.overrideMimeType("text/plain; charset=x-user-defined");
      xhr2.open('GET', `./raw_games/${wiiRomName}`);
      xhr2.send();
      xhr2.onload = function() {
        if (xhr2.status != 200) { // analyze HTTP status of the response
          console.error(`Error ${xhr2.status}: ${xhr2.statusText}`); // e.g. 404: Not Found
        } else { // show the result
          const wiiRom = xhr2.responseText
          main(linkRom, wiiRom)
        }
      }
    }
  }

  // function loadStuffAndExecute() {
  //   const options = {
  //       method: 'GET',
  //       headers:{'content-type': 'text/plain; charset=x-user-defined'},
  //       mode: 'no-cors'
  //     };
  //   fetch(`./raw_games/${romName}`, options)
  //     .then(res => {
  //       if(res.ok) {
  //         return res.text();
  //       } else {
  //         throw Error(`Request rejected with status ${res.status}`);
  //       }
  //     })
  //     .then(loadedROM => {
  //         main(loadedROM)
  //         // fetch (`./save_states/${saveStateName}`)
  //           // .then(response => response.json())
  //       // .then(j => {
  //         // downloadedSaveState = j
  //         // main(loadedROM)
  //       // })
  //     })
  //     .catch(console.error)
  // }

  // // loadStuffAndExecute()

}