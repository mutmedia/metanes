export default function loadRomsAndSaves(roms, savestates, callback) {
  const downloadedRoms = {}
  const downloadedSaves = {}
  for(const romKey of Object.keys(roms)) {
    const romName = roms[romKey]
    let xhr = new XMLHttpRequest();
    xhr.open('GET', `./raw_games/${romName}`);
    xhr.overrideMimeType("text/plain; charset=x-user-defined");
    xhr.send();

    xhr.onload = function() {
      if (xhr.status != 200) { // analyze HTTP status of the response
        console.error(`Error ${xhr.status}: ${xhr.statusText}`); // e.g. 404: Not Found
      } else { // show the result
        // alert(`Done, got ${xhr.response.length} bytes`); // response is the server response
        downloadedRoms[romKey] = xhr.responseText
        onLoad()
      }
    }
  }

  for(const saveKey of Object.keys(savestates)) {
    const saveStateName = savestates[saveKey]
    let xhr2 = new XMLHttpRequest();
    xhr2.open('GET', `./save_states/${saveStateName}`);
    // xhr2.overrideMimeType("text/plain; charset=x-user-defined");
    xhr2.send();
    xhr2.onload = function() {
      if (xhr2.status != 200) { // analyze HTTP status of the response
        console.error(`Error ${xhr2.status}: ${xhr2.statusText}`); // e.g. 404: Not Found
      } else { // show the result
        const saveState = JSON.stringify(JSON.parse(xhr2.responseText))
        downloadedSaves[saveKey] = saveState
        onLoad()
      }
    }
  }

  function onLoad() {
    if(Object.keys(downloadedSaves).length < Object.keys(savestates).length) return;
    if(Object.keys(downloadedRoms).length < Object.keys(roms).length) return;
    callback(downloadedRoms, downloadedSaves)
  }
}