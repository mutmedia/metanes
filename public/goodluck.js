import './lib/jsnes.js'
import downloadObjectAsJson from './lib/downloadObjectAsJson.js'

const romName = 'game1.nes'
const saveStateName = 'game1-close-to-automario.json'

const trackRestartEvent = new Event('track.restarted');
// const saveStateName = 'game1-pipe.json'

let FRAME = 1
const PLAYER_1 = 1
let dontUpdate = false

const main = (marioROM) => {
  let highScore = parseInt(window.localStorage.getItem("high_score")) || 566
  document.getElementById('highScore').innerHTML = highScore
  const KEYS = {
    88: [PLAYER_1, jsnes.Controller.BUTTON_A], // X
    89: [PLAYER_1, jsnes.Controller.BUTTON_B], // Y (Central European keyboard)
    90: [PLAYER_1, jsnes.Controller.BUTTON_B], // Z
    17: [PLAYER_1, jsnes.Controller.BUTTON_SELECT], // Right Ctrl
    13: [PLAYER_1, jsnes.Controller.BUTTON_START], // Enter
    38: [PLAYER_1, jsnes.Controller.BUTTON_UP], // Up
    40: [PLAYER_1, jsnes.Controller.BUTTON_DOWN], // Down
    37: [PLAYER_1, jsnes.Controller.BUTTON_LEFT], // Left
    39: [PLAYER_1, jsnes.Controller.BUTTON_RIGHT] // Right
  }


  const SCREEN_WIDTH = 800
  const SCREEN_HEIGHT = 800
  class NESampler {
    // public fields
    contextMod = null

    canvasMod = null

    constructor(width, height, canvasElementId = 'screen_mod') {
      this.canvasMod = document.getElementById('screen_mod')
      this.contextMod = this.canvasMod.getContext('2d')
      this.canvasMod.style.display = 'block'

      document.getElementById('loading').style.display = 'none'

      this.contextMod.fillStyle = 'blue'
      // set alpha to opaque
      this.contextMod.fillRect(0, 0, width, height)
    }

    Loop() {
      this.contextMod.clearRect(0, 0, this.canvasMod.width, this.canvasMod.height)
      // window.requestAnimationFrame(loop);


      // TODO: make this be a consistent framerate?
      window.setTimeout(() => this.Loop(), 1000 / 60)

      
      tracks.forEach(track => {
        // if(dontUpdate) return;
        track.nes.frame()
        // todo move to class?
        if(track.restartTrigger && track.restartTrigger()) {
            track.dispatchEvent(trackRestartEvent)
            track.restart()
        }
      })
    }
  }

  const nesampler = new NESampler()
  const contextMod = nesampler.contextMod
  const canvasMod = nesampler.canvasMod

  let tracks = []
  const NES_WIDTH = 256
  const NES_HEIGHT = 240
  function CreateTrack (settings, update, shader, sample) {
    // #region Canvas bullshit
    const canvasTrack = document.createElement('canvas')
    canvasTrack.width = NES_WIDTH
    canvasTrack.height = NES_HEIGHT
    const contextTrack = canvasTrack.getContext('2d')
    const imageDataTrack = contextTrack.getImageData(0, 0, NES_WIDTH, NES_HEIGHT)
    contextTrack.fillStyle = 'black'
    // set alpha to opaque
    contextTrack.fillRect(0, 0, 256, 240)

    // buffer to write on next animation frame
    let bufTrack = new ArrayBuffer(imageDataTrack.data.length)
    let buf8Track = new Uint8ClampedArray(bufTrack)
    let buf32Track = new Uint32Array(bufTrack)

    // Set alpha
    for (let i = 0; i < buf32Track.length; ++i) {
      buf32Track[i] = 0xff000000
    }
    // #endregion

    let track = canvasTrack
    track.drawSettings = settings
    track.draw = (context) => {
      const scaleX = track.drawSettings.width / NES_WIDTH
      const scaleY = track.drawSettings.height / NES_HEIGHT

      imageDataTrack.data.set(buf8Track)
      contextTrack.putImageData(imageDataTrack, 0, 0)

      context.save()
      context.scale(scaleX, scaleY)
      context.drawImage(canvasTrack, track.drawSettings.x, track.drawSettings.y)
      context.restore()
    }
    track.update = () => update(track)
    track.loaded = false
    track.frame = 0

    let nes = new jsnes.NES({
      onFrame: (buffer) => {
        update(track)
        for (let y = 0; y < NES_HEIGHT; ++y) {
          for (let x = 0; x < NES_WIDTH; ++x) {
            let i = (y * 256 + x)
            buf32Track[i] = shader(buffer, x, y)
          }
        }
        track.frame++

        track.draw(contextMod)
      },
      onAudioSample: function (left, right) {
      }
    })
    track.nes = nes

    if (settings.hasControl) {
      document.addEventListener('keyup', (e) => {
        const key = KEYS[e.keyCode]
        if (key) {
          nes.buttonUp(key[0], key[1])
          // e.preventDefault()
        }
      })
      document.addEventListener('keydown', (e) => {
        const key = KEYS[e.keyCode]
        if (key) {
          nes.buttonDown(key[0], key[1])
          // e.preventDefault()
        }
      })
    }

    track.nes.loadROM(marioROM)
    console.log('loaded rom')
    if (sample) {
      console.log("loading track", sample)
      track.nes.fromJSON(JSON.parse(sample.initialState))
      sample.initialState = JSON.stringify(track.nes.toJSON())
      if(sample.restartTrigger) {
        track.restartTrigger = () => sample.restartTrigger(track)
        track.onRestart
      }
      if(sample.onRestart) {
        track.onRestart = () => sample.onRestart(track)
      }

      if (sample.loopTime) {
        setInterval(() => {
          track.nes.fromJSON(JSON.parse(sample.initialState))
        }, sample.loopTime)
      }

      console.log('loaded sample')
    }

    track.restart = function() {
      if(track.onRestart) {
        track.onRestart()
      }
      if(sample) {
        track.nes.fromJSON(JSON.parse(sample.initialState))
      } else {
        track.nes.reset()
      }
    }

    track.loaded = true

    tracks.push(track)

    return track
  }

  const FRAMES_TO_SAVE = 30
  const savedFrames = new Array(FRAMES_TO_SAVE)
  const SaveFrame = (track) => {
    savedFrames[track.frame % FRAMES_TO_SAVE] = JSON.stringify(track.nes.toJSON())
  }

  const GetFrame = (currentFrame, delta) => {
    return savedFrames[(currentFrame - delta + FRAMES_TO_SAVE) % FRAMES_TO_SAVE]
  }

  const CreateKillTrack = () => CreateTrack(
    {
      x: Math.random() * (SCREEN_WIDTH - NES_WIDTH),
      y: Math.random() * (SCREEN_HEIGHT - NES_HEIGHT),
      width: NES_WIDTH,
      height: NES_HEIGHT
    },
    (track) => {
      // Do nothing
    },
    (buffer, x, y) => {
      let xd = x // (x + FRAME) % 256
      let yd = y // (y + FRAME) % 240
      let ri = (yd) * 256 + (xd)
      let transparency = Math.floor(Math.random() * 0xf0) << 24
      return (transparency) | buffer[ri]
      // return 0
    },
    {
      initialState: GetFrame(mainTrack.frame, 30),
      loopTime: 1000
    })

  let currentScore = 0
  let lastX = -100
  let totalX = 0
  const mainTrack = CreateTrack(
    {
      x: 0,
      y: 0,
      width: SCREEN_WIDTH,
      height: SCREEN_HEIGHT,
      hasControl: false
    },
    (track) => {
      SaveFrame(track)
      let screenX = track.nes.cpu.load(0x071D)
      
      if(lastX > 0) {
        const deltaX = screenX - lastX
        if(Math.abs(deltaX) > 50) {
          // eg 10 -> 240  => delta = -230 => -25
          if(deltaX < 0) {
            totalX -= (255 + deltaX)
          // eg 240 -> 10  => delta = 230 => +25
          } else {
            totalX += (255 - deltaX)
          }
        } else {
          totalX += deltaX
        }
      }

      // document.getElementById("currentScore").innerHTML = totalX
      if(totalX > currentScore) {
        currentScore = totalX
      }

      for(let i = 0; i < 6; i++) {
        var scoreAddress = 0x07DD + i
        track.nes.cpu.write(scoreAddress, parseInt("420"[i%3]))
      }

      track.nes.cpu.write(0x07EE, 6)
      track.nes.cpu.write(0x07ED, 9)

      document.getElementById("maxScore").innerHTML = currentScore
      lastX = screenX

      function Pause() {
        track.nes.buttonDown(jsnes.Controller.BUTTON_START)
      }
    },
    (buffer, x, y) => {
      let xd = x
      let yd = y
      let ri = (yd) * 256 + (xd)
      return 0xff000000 | buffer[ri]
    }
    ,{
      initialState: JSON.stringify(downloadedSaveState),
      restartTrigger: function(track) {
        return track.nes.cpu.load(MarioStateAddress) === MarioState_PlayerDies || 
                track.nes.cpu.load(MarioStateAddress) === MarioState_LeftmostOfScreen
      },
      onRestart: function(track) {
        CheckAndResetScoreData();
      }
    }
  )

  nesampler.Loop()

  let saveState
  let palette = 0;
  let fakeDeath = false;
  function toggleDead() {
    if(mainTrack.nes.cpu.load(MarioStateAddress) == MarioState_Dying) {
        mainTrack.nes.cpu.write(MarioStateAddress, MarioState_Normal) // normal
        fakeDeath = false
      } else {
        mainTrack.nes.cpu.write(MarioStateAddress, MarioState_Dying) //dying
        fakeDeath = true
      }
  }

  function randomizeMemory() {
    mainTrack.nes.cpu.write(Math.floor(Math.random() * 0x87F4), Math.floor(Math.random() * 256)) // normal
  }

  function CheckAndResetScoreData() {
    if(currentScore > highScore) {
      highScore = currentScore
      window.localStorage.setItem("high_score", highScore)
      document.getElementById('highScore').innerHTML = highScore
    }
    totalX = 0
    currentScore = 0
    lastX = -1
  }

  // mainTrack.addEventListener("track.restarted", CheckAndResetScoreData)

  document.getElementById("retry").onclick = function() {
    mainTrack.restart()
  }

  document.getElementById("toggleDead").onclick = function() {
    toggleDead()
  }

  document.getElementById("lucky").onclick = function() {
    randomizeMemory()
  }

  document.getElementById("changeMemory").onclick = function() {
    const address = parseInt(document.getElementById("changeMemoryAddress").value)
    const value = parseInt(document.getElementById("changeMemoryValue").value)
    console.log("changing memory", address, value)
    mainTrack.nes.cpu.write(address, value) // normal
  }

  document.addEventListener('keyup', (e) => {
    switch (e.keyCode) {
      case 49: // 2
        mainTrack.nes.cpu.write(MarioStateAddress) // normal
        break
      case 50: // 2
        break
      case 51: // 3
        mainTrack.nes.cpu.write(MarioStateAddress) //dying
        break
      case 52: // 4
        if(saveState !== null) {
          mainTrack.nes.fromJSON(JSON.parse(saveState))
        }
        break
      case 53: // 5
        saveState = JSON.stringify(mainTrack.nes.toJSON());
        break
      case 54: // 6
        downloadObjectAsJson(mainTrack.nes.toJSON())
        break
      case 55: // 7
        mainTrack.reset()
        break
      case 32: // space
      case 67: // C
        toggleDead()
        break
      case 79: // 0
        // kill player
        mainTrack.nes.cpu.write(0x07f8, 0)
        mainTrack.nes.cpu.write(0x07f9, 0)
        mainTrack.nes.cpu.write(0x07fa, 0)
        break;
      case 76: // P
        randomizeMemory()
        break
      case 80: // P
        mainTrack.nes.cpu.write(0x0773, palette++) //dying
        break
      case 82: // R
        // mainTrack.nes.fromJSON(JSON.parse(GetFrame(mainTrack.frame, 30)))
        mainTrack.restart();
        // CreateKillTrack()
        break
      }
    })
  }

const MarioStateAddress = 0x00E
const MarioState_LeftmostOfScreen = 0x00
const MarioState_ClimbingVine = 0x01
const MarioState_EnteringReversedLPipe = 0x02
const MarioState_GoingDownAPipe = 0x03
const MarioState_Autowalk = 0x04
const MarioState_Autowalk2 = 0x05
const MarioState_PlayerDies = 0x06
const MarioState_EnteringArea = 0x07
const MarioState_Normal = 0x08
const MarioState_TransformingFromSmallToLarge = 0x09
const MarioState_TransformingFromLargeToSmall = 0x0A
const MarioState_Dying = 0x0B
const MarioState_TransformingToFireMario = 0x0C

const MarioStateString = { // 0x00E
    0x00: "Leftmost of screen",
    0x01: "Climbing vine",
    0x02: "Entering reversed-L pipe",
    0x03: "Going down a pipe",
    0x04: "Autowalk",
    0x05: "Autowalk",
    0x06: "Player dies",
    0x07: "Entering area",
    0x08: "Normal",
    0x09: "Transforming from Small to Large (cannot move)",
    0x0A: "Transforming from Large to Small (cannot move)",
    0x0B: "Dying",
    0x0C: "Transforming to Fire Mario (cannot move)",
}


let downloadedSaveState = null
let xhr = new XMLHttpRequest();
xhr.open('GET', `./raw_games/${romName}`);
xhr.overrideMimeType("text/plain; charset=x-user-defined");
xhr.send();

// 4. This will be called after the response is received
xhr.onload = function() {
  if (xhr.status != 200) { // analyze HTTP status of the response
    console.error(`Error ${xhr.status}: ${xhr.statusText}`); // e.g. 404: Not Found
  } else { // show the result
    // alert(`Done, got ${xhr.response.length} bytes`); // response is the server response
    var loadedROM = xhr.responseText
    let xhr2 = new XMLHttpRequest();
    xhr2.open('GET', `./save_states/${saveStateName}`);
    // xhr2.overrideMimeType("text/plain; charset=x-user-defined");
    xhr2.send();
    xhr2.onload = function() {
      if (xhr2.status != 200) { // analyze HTTP status of the response
        console.error(`Error ${xhr2.status}: ${xhr2.statusText}`); // e.g. 404: Not Found
      } else { // show the result
        downloadedSaveState = JSON.parse(xhr2.responseText)
        main(loadedROM)
      }
    }
  }
}

// function loadStuffAndExecute() {
//   const options = {
//       method: 'GET',
//       headers:{'content-type': 'text/plain; charset=x-user-defined'},
//       mode: 'no-cors'
//     };
//   fetch(`./raw_games/${romName}`, options)
//     .then(res => {
//       if(res.ok) {
//         return res.text();
//       } else {
//         throw Error(`Request rejected with status ${res.status}`);
//       }
//     })
//     .then(loadedROM => {
//         main(loadedROM)
//         // fetch (`./save_states/${saveStateName}`)
//           // .then(response => response.json())
//       // .then(j => {
//         // downloadedSaveState = j
//         // main(loadedROM)
//       // })
//     })
//     .catch(console.error)
// }

// // loadStuffAndExecute()