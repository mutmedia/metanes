import './lib/jsnes.js'
import downloadObjectAsJson from './lib/downloadObjectAsJson.js'

import NESampler from './NESsampler/NESsampler.js'
import NEStrack from './NESsampler/NEStrack.js'

import KEYS from './NESsampler/inputMaps/onePlayerDefault.js'
// import { jsnes } from './lib/jsnes.js'

const ROMS = {
  doteater: 'doteater.nes',
  controlled: 'helicopter.nes',
}

const SAVE_STATES = {
  doteaterReady: 'doteaterready.json',
  helicopterStart: 'helicopter-started.json',
}

const YELLOW = 0xF7B400
const RED = 0x5F0000
const PINK = 0xD8B8F8
const ORANGE = 0xB15400
const BLUE = 0x3CABFF
const DEEPBLUE = 0x2052FE
const GHOST_KEYS = ['blue', 'red', 'pink', 'orange']

const gameData = {
  pellets: {
    ram: 0x006A,
    count: 0,
    delta: 0,
  },
  eater: {
    color: YELLOW,
    ramX: 0x001A,
    ramY: 0x001C,
    ramLives: 0x0067,
    lastPos: null,
    alive: true,
    powerup: false,
  },
  red: {
    name: 'red',
    color: RED,
    ramX: 0x001E,
    ramY: 0x0020,
    dir: {x: 0, y: 0},
    lastPos: null,
    alive: true,
  },
  pink: {
    name: 'pink',
    color: PINK,
    ramX: 0x0022,
    ramY: 0x0024,
    dir: {x: 0, y: 0},
    lastPos: null,
    alive: true,
  },
  blue: {
    name: 'blue',
    color: BLUE,
    ramX: 0x0026,
    ramY: 0x0028,
    dir: {x: 0, y: 0},
    lastPos: null,
    alive: true,
  },
  orange: {
    name: 'orange',
    color: ORANGE,
    ramX: 0x002A,
    ramY: 0x002C,
    dir: {x: 0, y: 0},
    lastPos: null,
    alive: true,
  }
}

function bgr2rgb(color) {
  return ((0xff0000 & color) >> 0x10) | (0xff00 & color) | ((0xff & color) << 0x10)
}

window.currentMemTest = 'eq'
window.checkMem = false
window.filterMem = null
// window.filterMem = window.localStorage.getItem('filtermem') || null
// if(window.filterMem != null) {
  // window.filterMem = JSON.parse(window.filterMem)
// }

let prevMem = 0;
function Compare(a, b, op) {
  switch(op) {
    case 'leq':
      return a <= b
      break;
    case 'geq':
      return a >= b
      break;
    case 'gt':
      return a > b
      break;
    case 'lt':
      return a < b
      break;
    case 'neq':
      return a != b
      break;
    case 'eq':
      return a == b
      break;
  }

  console.error(`${op} does not exist`);
  return true
}

function DoMemoryCheck(track) {
  const mem = track.nes.cpu.mem
  if(window.filterMem == null) {
    window.filterMem = mem.map((m, i) => {
      return [m, null]
    })
  } else {
    window.filterMem = window.filterMem.map((m, i) => {
      if(m == null) return null;
      if(Compare(m[0], mem[i], window.currentMemTest)) {
        return [mem[i], mem[i] - m[0]]
      } else {
        return null
      }
    })

    const mappedMem = window.filterMem.reduce((p, c, i) => {
      if(c != null) {
        p[i.toString(16)] = c
      }
      return p
    }, {})

    console.log(mappedMem)
  }
}

const main = (roms, savestates) => {
  const SCREEN_WIDTH = 800
  const SCREEN_HEIGHT = 800

  // const nesSamplerLink = new NESampler()
  const doteaterAndGameSampler = new NESampler()
  const doteaterRom = roms.doteater
  const notmeleeRom = roms.controlled
  const downloadedSaveState = savestates.doteaterReady

  let playerIndex = 0
  let player = gameData[GHOST_KEYS[playerIndex]]

  const controlledTrack = new NEStrack(
  { // parameters
    x: 1*SCREEN_WIDTH/3,
    y: 0,
    width: 2*SCREEN_WIDTH/3,
    height: 2*SCREEN_HEIGHT/3,
    hasControl: true,
    controller: KEYS,
  },
  function update() {
  },
  function shader(buffer, x, y) { // shader
    let ri = (y) * 256 + (x)
    let c = bgr2rgb(buffer[ri])
    if(c === 0xBC1900) {
      c = player.color
    }
    // if (c === 0xFF6D47) {
    // }
    // else if(c !== rival.color && (c === PINK || c === BLUE || c === RED || c === ORANGE)) {
    //   c = 0xaaaaaa
    // }
    // else if(c === YELLOW) {
    //   c = player.color
    // }
    // else if(c === 0x0) {
    //   return 0x0
    // }
    return 0xff000000 | bgr2rgb(c)
  },
  {
    rom: notmeleeRom,
    initialState: savestates.helicopterStart,
    restartTrigger: function() {
    },
    onRestart: function() {
    }
  })

  const doteaterScale = 1.2/3
  const doteaterOffset = {
    x: 0,
    y: 100
  }
  const doteaterSpriteOffset = {
    x: -4,
    y: 8,
  }

  const doteaterTrack = new NEStrack(
    { // parameters
      x: doteaterOffset.x,
      y: doteaterOffset.y,
      width: SCREEN_WIDTH * doteaterScale,
      height: SCREEN_HEIGHT * doteaterScale,
      hasControl: true,
      controller: KEYS,
    },
    function update() {
      const pellets = this.nes.cpu.load(gameData.pellets.ram)
      const sprite = this.nes.cpu.load(0x0036)
      const powerup = sprite === 31 || sprite === 30

      if(player.dir.x > 0) {
        controlledTrack.nes.buttonDown(1, jsnes.Controller.BUTTON_RIGHT)
        controlledTrack.nes.buttonUp(1, jsnes.Controller.BUTTON_LEFT)
      } else if(player.dir.x < 0) {
        controlledTrack.nes.buttonDown(1, jsnes.Controller.BUTTON_LEFT)
        controlledTrack.nes.buttonUp(1, jsnes.Controller.BUTTON_RIGHT)
      } else {
        controlledTrack.nes.buttonUp(1, jsnes.Controller.BUTTON_RIGHT)
        controlledTrack.nes.buttonUp(1, jsnes.Controller.BUTTON_LEFT)
      }

      if(player.dir.y > 0) {
        controlledTrack.nes.buttonDown(1, jsnes.Controller.BUTTON_DOWN)
        controlledTrack.nes.buttonUp(1, jsnes.Controller.BUTTON_UP)
      } else if(player.dir.y < 0) {
        controlledTrack.nes.buttonDown(1, jsnes.Controller.BUTTON_UP)
        controlledTrack.nes.buttonUp(1, jsnes.Controller.BUTTON_DOWN)
      } else {
        controlledTrack.nes.buttonUp(1, jsnes.Controller.BUTTON_DOWN)
        controlledTrack.nes.buttonUp(1, jsnes.Controller.BUTTON_UP)
      }

      if(pellets < gameData.pellets.count) {
        controlledTrack.nes.buttonDown(1, jsnes.Controller.BUTTON_B)
      } else {
        controlledTrack.nes.buttonUp(1, jsnes.Controller.BUTTON_B)
      }

      if(powerup) {
        controlledTrack.nes.buttonDown(1, jsnes.Controller.BUTTON_A)
      } else {
        controlledTrack.nes.buttonUp(1, jsnes.Controller.BUTTON_A)
      }

      if(!gameData.eater.lastPos) {
        gameData.eater.lastPos = {
          x: 0, y: 0
        }
      }
      gameData.eater.lastPos.x = this.nes.cpu.load(gameData.eater.ramX)
      gameData.eater.lastPos.y = this.nes.cpu.load(gameData.eater.ramY)
      gameData.pellets.delta = Math.max(gameData.pellets.count - pellets, 0)
      gameData.pellets.count = pellets
      gameData.eater.powerup = powerup
      gameData.eater.alive = this.nes.cpu.load(0x0032) < 18

      for(const ghostKey of GHOST_KEYS) {
        const ghost = gameData[ghostKey]

        const ghostPos = {
          x: this.nes.cpu.load(ghost.ramX),
          y: this.nes.cpu.load(ghost.ramY)
        }

        if(ghost.lastPos){
          ghost.dir.x = Math.sign(ghostPos.x - ghost.lastPos.x)
          ghost.dir.y = Math.sign(ghostPos.y - ghost.lastPos.y)
        }

        ghost.lastPos = ghostPos
      }
    },
    function shader(buffer, x, y) { // shader
      let ri = (y) * 256 + (x)
      let c = bgr2rgb(buffer[ri])
      if(x > 175) {
        c = 0x0
      } else if (c === 0xFF6D47) {
        c = player.color
      }
      else if(c !== player.color && (c === PINK || c === BLUE || c === RED || c === ORANGE)) {
        c = 0xaaaaaa
      }
      // else if(c === YELLOW) {
      //   // c = player.color
      // }
      else if(c === 0x0) {
        return 0x0
      }
      return 0xff000000 | bgr2rgb(c)
    }
    ,{ // sample
      rom: doteaterRom,
      initialState: savestates.doteaterReady,
      restartTrigger: function() {
        return !gameData.eater.alive
        // return this.nes.cpu.load(gameData.eater.ramLives) < 2
      },
      onRestart: function() {
      }
    })


  doteaterAndGameSampler.tracks.push(doteaterTrack)
  doteaterAndGameSampler.tracks.push(controlledTrack)

  doteaterAndGameSampler.loop("rgba(0, 0, 0, 1", function() {
    const ctx = this.contextMod
    ctx.save()

    const scale = {
      x: doteaterScale * SCREEN_WIDTH / NEStrack.WIDTH,
      y: doteaterScale * SCREEN_HEIGHT / NEStrack.HEIGHT,
    }

    { // ghost input
      const color = `#${player.color.toString(16)}`
      ctx.fillStyle = color
      ctx.strokeStyle = color
      ctx.lineWidth = 4
      ctx.beginPath();

      const startPos = {
        x: doteaterOffset.x + (player.lastPos.x + doteaterSpriteOffset.x) * scale.x,
        y: doteaterOffset.y + (player.lastPos.y + doteaterSpriteOffset.y) * scale.y,
      }

      ctx.moveTo(startPos.x, startPos.y)
      ctx.lineTo(startPos.x, 34)
      ctx.lineTo(SCREEN_WIDTH/3 + 18, 34)
      ctx.rect(SCREEN_WIDTH/3 + 18, 17, 500, 500)
      ctx.stroke()
    }

    // pellet input
    if(gameData.pellets.delta > 0) {
      const color = `#${gameData.eater.color.toString(16)}`
      ctx.fillStyle = color
      ctx.strokeStyle = color
      ctx.lineWidth = 4
      ctx.beginPath();

      const eater = gameData.eater

      const startPos = {
        x: doteaterOffset.x + (eater.lastPos.x + doteaterSpriteOffset.x) * scale.x,
        y: doteaterOffset.y + (eater.lastPos.y + doteaterSpriteOffset.y) * scale.y,
      }

      ctx.moveTo(startPos.x, startPos.y)
      ctx.lineTo(startPos.x, 500)
      ctx.lineTo(SCREEN_WIDTH/3 + 18, 500)

      ctx.rect(SCREEN_WIDTH/3 + 17, 16, 502, 502)
      ctx.stroke()
    }

    ctx.restore()
  })

  let mainTrack = controlledTrack

  let saveState

  window.switchControllingGhost = function() {
    playerIndex = (playerIndex + 1) % GHOST_KEYS.length
    player = gameData[GHOST_KEYS[playerIndex]]
  }

  window.restartHeli = function() {
    controlledTrack.restart()
  }

  doteaterAndGameSampler.canvasMod.addEventListener('pointerdown', (e) => {
    const nesX = (e.offsetX-doteaterOffset.x) / (SCREEN_WIDTH * doteaterScale) * NEStrack.WIDTH + doteaterSpriteOffset.x

    const nesY = (e.offsetY-doteaterOffset.y) / (SCREEN_HEIGHT * doteaterScale) * NEStrack.HEIGHT + doteaterSpriteOffset.y

    let minDist = 420420
    let closest = null
    for(const ghostKey of GHOST_KEYS) {
      const ghost = gameData[ghostKey]
      const dx = ghost.lastPos.x - nesX
      const dy = ghost.lastPos.y - nesY
      const dist = dx*dx + dy*dy
      if(dist < minDist) {
        closest = ghostKey
        playerIndex = GHOST_KEYS.indexOf(ghostKey)
        player = gameData[GHOST_KEYS[playerIndex]]
        minDist = dist
      }
    }

    console.log('closest ghost', )
  })

  document.addEventListener('keyup', (e) => {
    switch (e.keyCode) {
      case 49: // 1 Check memory
        DoMemoryCheck(mainTrack)
        break
      case 50: // 2 Save memory
        window.localStorage.setItem(
          'filtermem',
          JSON.stringify(window.filterMem))
        break
      case 51: // 3 Clear memory
        window.localStorage.setItem(
          'filtermem',
          null)
        window.filterMem = null
        break
      case 52: // 4
        if(saveState !== null) {
          mainTrack.nes.fromJSON(JSON.parse(saveState))
        }
        break
      case 53: // 5
        saveState = JSON.stringify(mainTrack.nes.toJSON());
        break
      case 54: // 6
        downloadObjectAsJson(mainTrack.nes.toJSON())
        break
      case 55: // 7
        mainTrack.reset()
        break
      case 32: // space
        // window.checkMem = false;
        playerIndex = (playerIndex + 1) % GHOST_KEYS.length
        player = gameData[GHOST_KEYS[playerIndex]]
      case 67: // C
        break
      case 48: // 0
        // window.Compare
        if(window.currentMemTest === 'eq') {
          window.currentMemTest = 'neq'
        } else {
          window.currentMemTest = 'eq'
        }
        console.log("CURRENT OP", window.currentMemTest)

        break;
      case 79: // O
        mainTrack.executeFrame(true)
        break
      case 80: // P
        mainTrack.togglePause()
        break
      case 82: // R
        // mainTrack.nes.fromJSON(JSON.parse(GetFrame(mainTrack.frame, 30)))
        mainTrack.restart();
        // CreateKillTrack()
        break
      }
    })
  }

document.getElementById("playButton").onclick = function() {
  Setup(ROMS, SAVE_STATES);
}

function Setup(roms, savestates) {
  const downloadedRoms = {}
  const downloadedSaves = {}
  for(const romKey of Object.keys(roms)) {
    const romName = roms[romKey]
    let xhr = new XMLHttpRequest();
    xhr.open('GET', `./raw_games/${romName}`);
    xhr.overrideMimeType("text/plain; charset=x-user-defined");
    xhr.send();

    xhr.onload = function() {
      if (xhr.status != 200) { // analyze HTTP status of the response
        console.error(`Error ${xhr.status}: ${xhr.statusText}`); // e.g. 404: Not Found
      } else { // show the result
        // alert(`Done, got ${xhr.response.length} bytes`); // response is the server response
        downloadedRoms[romKey] = xhr.responseText
        onLoad()
      }
    }
  }

  for(const saveKey of Object.keys(savestates)) {
    const saveStateName = savestates[saveKey]
    let xhr2 = new XMLHttpRequest();
    xhr2.open('GET', `./save_states/${saveStateName}`);
    // xhr2.overrideMimeType("text/plain; charset=x-user-defined");
    xhr2.send();
    xhr2.onload = function() {
      if (xhr2.status != 200) { // analyze HTTP status of the response
        console.error(`Error ${xhr2.status}: ${xhr2.statusText}`); // e.g. 404: Not Found
      } else { // show the result
        const saveState = JSON.stringify(JSON.parse(xhr2.responseText))
        downloadedSaves[saveKey] = saveState
        onLoad()
      }
    }
  }

  function onLoad() {
    if(Object.keys(downloadedSaves).length < Object.keys(savestates).length) return;
    if(Object.keys(downloadedRoms).length < Object.keys(roms).length) return;
    main(downloadedRoms, downloadedSaves)
  }
}