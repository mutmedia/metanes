const NES_WIDTH = 256
const NES_HEIGHT = 240

const trackRestartEvent = new Event('track.restarted');

export default class NEStrack {
  static WIDTH = NES_WIDTH
  static HEIGHT = NES_HEIGHT

  prevMem = []
  drawSettings = null
  frame = 0
  loaded = false

  constructor(settings, update, shader, sample) {
    // #region Canvas bullshit
    this.canvasTrack = document.createElement('canvas')
    this.canvasTrack.width = NES_WIDTH
    this.canvasTrack.height = NES_HEIGHT
    this.contextTrack = this.canvasTrack.getContext('2d')
    this.imageDataTrack = this.contextTrack.getImageData(0, 0, NES_WIDTH, NES_HEIGHT)
    this.contextTrack.fillStyle = 'black'
    // set alpha to opaque
    this.contextTrack.fillRect(0, 0, 256, 240)

    // buffer to write on next animation frame
    this.bufTrack = new ArrayBuffer(this.imageDataTrack.data.length)
    this.buf8Track = new Uint8ClampedArray(this.bufTrack)
    this.buf32Track = new Uint32Array(this.bufTrack)

    // Set alpha
    for (let i = 0; i < this.buf32Track.length; ++i) {
      this.buf32Track[i] = 0xffff00ff
    }
    // #endregion

    this._update = update.bind(this)
    this.shader = shader.bind(this)
    this.sample = sample

    this.drawSettings = settings

    this.nes = new jsnes.NES({
      onFrame: (buffer) => {
        for (let y = 0; y < NES_HEIGHT; ++y) {
          for (let x = 0; x < NES_WIDTH; ++x) {
            let i = (y * 256 + x)
            this.buf32Track[i] = this.shader(buffer, x, y)
          }
        }

        this.frame++
      },
      onAudioSample: function (left, right) {
      }
    })

    if (settings.hasControl) {
      document.addEventListener('keyup', (e) => {
        const key = settings.controller[e.keyCode]
        if (key) {
          this.nes.buttonUp(key[0], key[1])
          // e.preventDefault()
        }
      })
      document.addEventListener('keydown', (e) => {
        const key = settings.controller[e.keyCode]
        if (key) {
          this.nes.buttonDown(key[0], key[1])
          // e.preventDefault()
        }
      })
    }

    console.log('loading rom...')
    this.nes.loadROM(sample.rom)
    this.nes.frame()
    console.log('loaded rom!')
    if (sample) {
      console.log("loading track...") //, sample)

      if(sample.initialState) {
        this.nes.fromJSON(JSON.parse(sample.initialState))
      }

      sample.initialState = JSON.stringify(this.nes.toJSON())

      if(sample.restartTrigger) {
        this.restartTrigger = sample.restartTrigger.bind(this)
      }

      if(sample.onRestart) {
        this.onRestart = sample.onRestart.bind(this)
      }

      if (sample.loopTime) {
        setInterval(() => {
          this.nes.fromJSON(JSON.parse(sample.initialState))
        }, sample.loopTime)
      }

      console.log('loaded sample')
    }

    this.isPlaying = true
    this.loaded = true
  }

  draw(context) {
    const scaleX = this.drawSettings.width / NES_WIDTH
    const scaleY = this.drawSettings.height / NES_HEIGHT

    this.imageDataTrack.data.set(this.buf8Track)
    this.contextTrack.putImageData(this.imageDataTrack, 0, 0)

    context.save()
    context.scale(scaleX, scaleY)
    context.drawImage(this.canvasTrack, this.drawSettings.x/scaleX, this.drawSettings.y/scaleY)
    context.restore()
  }

  update(force) {
    if(force || this.isPlaying) {
      this.nes.frame()
    }
    this._update()
  }

  executeFrame(context, force) {
    this.update(force)
    this.draw(context)

    if(this.restartTrigger && this.restartTrigger()) {
        this.canvasTrack.dispatchEvent(trackRestartEvent)
        this.restart()
    }

    this.prevMem = this.nes.cpu.mem.map(a => a)
  }

  restart() {
    if(this.onRestart) {
      this.onRestart()
    }
    if(this.sample) {
      this.nes.fromJSON(JSON.parse(this.sample.initialState))
    } else {
      this.nes.reset()
    }
  }

  unpause() {
    this.isPlaying = true
  }

  pause() {
    this.isPlaying = false
  }

  togglePause() {
    this.isPlaying = !this.isPlaying
  }
}
