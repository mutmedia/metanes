const GAME_FPS = 60

export default class NESsampler {
  // public fields
  contextMod = null

  canvasMod = null

  constructor(width, height, canvasElementId = 'screen_mod') {
    this.canvasMod = document.getElementById(canvasElementId)
    this.contextMod = this.canvasMod.getContext('2d')
    this.canvasMod.style.display = 'block'

    document.getElementById('loading').style.display = 'none'
    document.getElementById('videoGameContainer').style.display = 'block'

    this.contextMod.fillStyle = 'blue'
    // set alpha to opaque
    this.contextMod.fillRect(0, 0, width, height)
    this.tracks = []
  }

  createTrack() {

  }

  loop(clearColor = "rgba(0, 0, 0, 1.0)", customDraw) {
    // window.requestAnimationFrame(loop);

    // TODO: make this be a consistent framerate?
    window.setTimeout(() => this.loop(clearColor, customDraw), 1000 / GAME_FPS)

    this.contextMod.fillStyle = clearColor
    this.contextMod.fillRect(0, 0, this.canvasMod.width, this.canvasMod.height)
    this.tracks.forEach(track => {
        track.executeFrame(this.contextMod)
    })

    if (customDraw) {
      customDraw = customDraw.bind(this)
      customDraw()
    }
  }
}