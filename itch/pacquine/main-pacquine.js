import './lib/jsnes.js'
import downloadObjectAsJson from './lib/downloadObjectAsJson.js'

import NESampler from './NESsampler/NESsampler.js'
import NEStrack from './NESsampler/NEStrack.js'

import KEYS from './NESsampler/inputMaps/onePlayerDefault.js'
// import { jsnes } from './lib/jsnes.js'

const doteaterRomName = 'doteater.nes'
const saveStateName = 'doteaterready.json'

const YELLOW = 0xF7B400
const RED = 0x5F0000
const PINK = 0xD8B8F8
const ORANGE = 0xB15400
const BLUE = 0x3CABFF
const DEEPBLUE = 0x2052FE

const PLAYER_ORDER = ['red', 'pink', 'blue', 'orange']
const CONTROL_TYPE_DIRECTION = 'direction'
const CONTROL_TYPE_ABSOLUTE = 'absolute'
let CONTROL_TYPE = CONTROL_TYPE_DIRECTION

window.switchControlType = function() {
  if(CONTROL_TYPE === CONTROL_TYPE_ABSOLUTE) {
    CONTROL_TYPE = CONTROL_TYPE_DIRECTION
  }
  else if(CONTROL_TYPE === CONTROL_TYPE_DIRECTION) {
    CONTROL_TYPE = CONTROL_TYPE_ABSOLUTE
  }

  document.getElementById('currentMode').innerHTML = CONTROL_TYPE

}

const gameData = {
  eater: {
    color: YELLOW,
    ramX: 0x001A,
    ramY: 0x001C,
    ramLives: 0x0067,
    lastPosPerPlayer: {
      red: null,
      pink: null,
      blue: null,
      orange: null,
    },
  },
  red: {
    name: 'red',
    color: RED,
    ramX: 0x001E,
    ramY: 0x0020,
    dir: {x: 0, y: 0},
    lastPos: null,
    alive: true,
  },
  pink: {
    name: 'pink',
    color: PINK,
    ramX: 0x0022,
    ramY: 0x0024,
    dir: {x: 0, y: 0},
    lastPos: null,
    alive: true,
  },
  blue: {
    name: 'blue',
    color: BLUE,
    ramX: 0x0026,
    ramY: 0x0028,
    dir: {x: 0, y: 0},
    lastPos: null,
    alive: true,
  },
  orange: {
    name: 'orange',
    color: ORANGE,
    ramX: 0x002A,
    ramY: 0x002C,
    dir: {x: 0, y: 0},
    lastPos: null,
    alive: true,
  }
}

function bgr2rgb(color) {
  return ((0xff0000 & color) >> 0x10) | (0xff00 & color) | ((0xff & color) << 0x10)
}

window.currentMemTest = 'eq'
window.checkMem = false
window.filterMem = null
// window.filterMem = window.localStorage.getItem('filtermem') || null
// if(window.filterMem != null) {
  // window.filterMem = JSON.parse(window.filterMem)
// }

let prevMem = 0;
function Compare(a, b) {
  const op = window.currentMemTest
  switch(op) {
    case 'leq':
      return a <= b
      break;
    case 'geq':
      return a >= b
      break;
    case 'gt':
      return a > b
      break;
    case 'lt':
      return a < b
      break;
    case 'neq':
      return a != b
      break;
    case 'eq':
      return a == b
      break;
  }

  console.error(`${op} does not exist`);
  return true
}

const main = (doteaterRom, downloadedSaveState) => {
  const SCREEN_WIDTH = 800
  const SCREEN_HEIGHT = 800

  // const nesSamplerLink = new NESampler()
  const doteaterSamplerMain = new NESampler()

  function DoMemoryCheck(track) {
    const mem = track.nes.cpu.mem
    if(window.filterMem == null) {
      window.filterMem = mem.map((m, i) => {
        return [m, null]
      })
    } else {
      window.filterMem = window.filterMem.map((m, i) => {
        if(m == null) return null;
        if(Compare(m[0], mem[i])) {
          return [mem[i], mem[i] - m[0]]
        } else {
          return null
        }
      })

      const mappedMem = window.filterMem.reduce((p, c, i) => {
        if(c != null) {
          p[i.toString(16)] = c
        }
        return p
      }, {})

      console.log(mappedMem)
    }
  }

  const numPlayers = PLAYER_ORDER.length

  function createGhostTrack(x, y, playerIndex) {
    const rivalIndex = (playerIndex+1) % numPlayers
    const playerKey = PLAYER_ORDER[playerIndex]
    const player = gameData[playerKey]
    let playerEater = gameData.eater.lastPosPerPlayer[playerKey]
    const rival = gameData[PLAYER_ORDER[rivalIndex]]
    // const rivalEater = gameData.eater.lastPosPerPlayer[rivalI]
    return new NEStrack(
      {
        x: x,
        y: y,
        width: SCREEN_WIDTH/2,
        height: SCREEN_HEIGHT/2,
        hasControl: false,
      },
      function update() {
        const rivalPos = {
          x: this.nes.cpu.load(rival.ramX),
          y: this.nes.cpu.load(rival.ramY)
        }

        player.alive = this.nes.cpu.load(0x0032) < 18

        switch(CONTROL_TYPE) {
          case CONTROL_TYPE_DIRECTION:
            if(player.dir.x > 0) {
              this.nes.buttonDown(1, jsnes.Controller.BUTTON_RIGHT)
              this.nes.buttonUp(1, jsnes.Controller.BUTTON_LEFT)
            } else if(player.dir.x < 0) {
              this.nes.buttonDown(1, jsnes.Controller.BUTTON_LEFT)
              this.nes.buttonUp(1, jsnes.Controller.BUTTON_RIGHT)
            } else {
              this.nes.buttonUp(1, jsnes.Controller.BUTTON_RIGHT)
              this.nes.buttonUp(1, jsnes.Controller.BUTTON_LEFT)
            }

            if(player.dir.y > 0) {
              this.nes.buttonDown(1, jsnes.Controller.BUTTON_DOWN)
              this.nes.buttonUp(1, jsnes.Controller.BUTTON_UP)
            } else if(player.dir.y < 0) {
              this.nes.buttonDown(1, jsnes.Controller.BUTTON_UP)
              this.nes.buttonUp(1, jsnes.Controller.BUTTON_DOWN)
            } else {
              this.nes.buttonUp(1, jsnes.Controller.BUTTON_DOWN)
              this.nes.buttonUp(1, jsnes.Controller.BUTTON_UP)
            }
            break

          case CONTROL_TYPE_ABSOLUTE:
          default:
            if(player.lastPos && player.lastPos.x + player.lastPos.y !== 0) {
              this.nes.cpu.write(gameData.eater.ramX, player.lastPos.x)
              this.nes.cpu.write(gameData.eater.ramY, player.lastPos.y)
            }
            this.nes.buttonUp(1, jsnes.Controller.BUTTON_DOWN)
            this.nes.buttonUp(1, jsnes.Controller.BUTTON_UP)
            this.nes.buttonUp(1, jsnes.Controller.BUTTON_RIGHT)
            this.nes.buttonUp(1, jsnes.Controller.BUTTON_LEFT)
            break
        }

        if(!playerEater) {
          gameData.eater.lastPosPerPlayer[playerKey] = {
            x: 0, y: 0
          }
          playerEater = gameData.eater.lastPosPerPlayer[PLAYER_ORDER[playerIndex]]
        }
        playerEater.x = this.nes.cpu.load(gameData.eater.ramX)
        playerEater.y = this.nes.cpu.load(gameData.eater.ramY)

        if(rival.lastPos){
          rival.dir.x = Math.sign(rivalPos.x - rival.lastPos.x)
          rival.dir.y = Math.sign(rivalPos.y - rival.lastPos.y)
        }

        rival.lastPos = rivalPos
      },
      function shader(buffer, x, y) { // shader
        let ri = (y) * 256 + (x)
        let c = bgr2rgb(buffer[ri])
        if (c === 0xFF6D47) {
          c = player.color
        }
        else if(c !== rival.color && (c === PINK || c === BLUE || c === RED || c === ORANGE)) {
          c = 0xaaaaaa
        }
        else if(c === YELLOW) {
          c = player.color
        }
        else if(c === 0x0) {
          return 0x0
        }
        return 0xff000000 | bgr2rgb(c)
      }
      ,{
        rom: doteaterRom,
        initialState: downloadedSaveState && JSON.stringify(downloadedSaveState),
        restartTrigger: function() {
          return this.nes.cpu.load(gameData.eater.ramLives) < 2
        },
        onRestart: function() {
        }
      }
    )
  }

  function calculateOffset(i) {
    return {
      x: Math.floor((i % 3) > 0 ? 1 : 0) * SCREEN_WIDTH/2,
      y: Math.floor(i / 2) * SCREEN_HEIGHT/2
    }
  }

  for(let i=0; i < numPlayers; i++) {
    const {x, y} = calculateOffset(i)
    const playerTrack = createGhostTrack(x, y, i)
    doteaterSamplerMain.tracks.push(playerTrack)
  }

  // doteaterSamplerMain.tracks.push(doteaterTrackRed)
  doteaterSamplerMain.loop("rgba(0, 0, 0, 1", function() {
    const ctx = this.contextMod
    ctx.save()

    for(let i=0; i < numPlayers; i++) {
      const offsetX = -4
      const offsetY = 8
      const pI = i
      const eI = (pI - 1 + numPlayers) % numPlayers
      const playerKey = PLAYER_ORDER[pI]
      const {x: pox, y: poy} = calculateOffset(pI)
      const {x: eox, y: eoy} = calculateOffset(eI)

      const color = `#${gameData[playerKey].color.toString(16)}`
      ctx.fillStyle = color
      ctx.strokeStyle = color
      ctx.lineWidth = 5
      ctx.beginPath();

      const scaleX = (SCREEN_WIDTH/2) / NEStrack.WIDTH
      const scaleY = (SCREEN_HEIGHT/2) / NEStrack.HEIGHT

      const {x: plpx, y: plpy} = gameData.eater.lastPosPerPlayer[playerKey]
      const {x: elpx, y: elpy} = gameData[playerKey].lastPos
      if(elpx + elpy  == 0) continue
      if(plpx == 56 && plpy == 87) continue
      if(!gameData[playerKey].alive) continue

      const startPosX = pox + (plpx + offsetX) * scaleX
      const startPosY = poy + (plpy + offsetY) * scaleY
      const endPosX = eox + (elpx + offsetX) * scaleX
      const endPosY = eoy + (elpy + offsetY) * scaleY

      ctx.moveTo(startPosX, startPosY)
      ctx.lineTo(endPosX, endPosY)
      ctx.stroke()
    }
    ctx.restore()
  })

  let mainTrack = doteaterSamplerMain.tracks[0]

  let saveState

  document.addEventListener('keyup', (e) => {
    switch (e.keyCode) {
      case 49: // 1 Check memory
        DoMemoryCheck(mainTrack)
        break
      case 50: // 2 Save memory
        window.localStorage.setItem(
          'filtermem',
          JSON.stringify(window.filterMem))
        break
      case 51: // 3 Clear memory
        window.localStorage.setItem(
          'filtermem',
          null)
        window.filterMem = null
        break
      case 52: // 4
        if(saveState !== null) {
          mainTrack.nes.fromJSON(JSON.parse(saveState))
        }
        break
      case 53: // 5
        saveState = JSON.stringify(mainTrack.nes.toJSON());
        break
      case 54: // 6
        downloadObjectAsJson(mainTrack.nes.toJSON())
        break
      case 55: // 7
        mainTrack.reset()
        break
      case 32: // space
        // window.checkMem = false;
      case 67: // C
        break
      case 48: // 0
        window.Compare
        if(window.currentMemTest === 'eq') {
          window.currentMemTest = 'neq'
        } else {
          window.currentMemTest = 'eq'
        }
        console.log("CURRENT OP", window.currentMemTest)

        break;
      case 79: // O
        mainTrack.executeFrame(true)
        break
      case 80: // P
        mainTrack.togglePause()
        break
      case 82: // R
        // mainTrack.nes.fromJSON(JSON.parse(GetFrame(mainTrack.frame, 30)))
        mainTrack.restart();
        // CreateKillTrack()
        break
      }
    })
  }

document.getElementById("playButton").onclick = function() {
  Setup();
}

function Setup() {

  let xhr = new XMLHttpRequest();
  xhr.open('GET', `./raw_games/${doteaterRomName}`);
  xhr.overrideMimeType("text/plain; charset=x-user-defined");
  xhr.send();

  xhr.onload = function() {
    if (xhr.status != 200) { // analyze HTTP status of the response
      console.error(`Error ${xhr.status}: ${xhr.statusText}`); // e.g. 404: Not Found
    } else { // show the result
      // alert(`Done, got ${xhr.response.length} bytes`); // response is the server response
      const doteaterRom = xhr.responseText
      // main(doteaterRom)
      if(saveStateName) {
        let xhr2 = new XMLHttpRequest();
        xhr2.open('GET', `./save_states/${saveStateName}`);
        // xhr2.overrideMimeType("text/plain; charset=x-user-defined");
        xhr2.send();
        xhr2.onload = function() {
          if (xhr2.status != 200) { // analyze HTTP status of the response
            console.error(`Error ${xhr2.status}: ${xhr2.statusText}`); // e.g. 404: Not Found
          } else { // show the result
            const saveState = JSON.parse(xhr2.responseText)
            main(doteaterRom, saveState)
          }
        }
      }
    }
  }

  // function loadStuffAndExecute() {
  //   const options = {
  //       method: 'GET',
  //       headers:{'content-type': 'text/plain; charset=x-user-defined'},
  //       mode: 'no-cors'
  //     };
  //   fetch(`./raw_games/${romName}`, options)
  //     .then(res => {
  //       if(res.ok) {
  //         return res.text();
  //       } else {
  //         throw Error(`Request rejected with status ${res.status}`);
  //       }
  //     })
  //     .then(loadedROM => {
  //         main(loadedROM)
  //         // fetch (`./save_states/${saveStateName}`)
  //           // .then(response => response.json())
  //       // .then(j => {
  //         // downloadedSaveState = j
  //         // main(loadedROM)
  //       // })
  //     })
  //     .catch(console.error)
  // }

  // // loadStuffAndExecute()

}